package igu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import logica.Actividad;
import logica.Centro;
import logica.Comprobaciones;
import logica.Instalacion;
import logica.Lista;
import logica.Monitor;
import logica.Reserva;
import logica.ReservaCancelada;
import logica.Socio;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import javax.swing.ListSelectionModel;

public class VentanaPrincipal extends JFrame {

	private static final long serialVersionUID = 1L;	
	
	LocalDateTime lol;
	private int total;
	String b;
	DefaultListModel<String> personas = new DefaultListModel<String>();
	
	private static Centro centro;	
	private JPanel contentPane;
	public UtilDateModel model;
	public JDatePanelImpl datePanel;
	public JDatePickerImpl datePicker;
	private final ButtonGroup buttonGroup = new ButtonGroup();	
	private final ButtonGroup buttonGroup2 = new ButtonGroup();
	private JPanel panelListInstalaciones;
	private JPanel panelInstalacionFecha;
	private JLabel lblInstalacionesDisponibles;
	private JPanel panelHoraReserva;
	private JPanel panelHoras;
	private JScrollPane scrollPaneHoras;
	private JTextArea txtLeyenda;
	private JPanel panelReservas;
	private JLabel lblIntroduzcaHorasDeseadas;
	private JLabel lblDesde;
	private JLabel lblHasta;
	private JLabel lblInformacionPersonal;
	private JLabel lblID;
	private JLabel lblFormaDePago;
	private JRadioButton rdbtnEfectivo;
	private JRadioButton rdbtnCuota;
	private JComboBox<LocalTime> comboBoxDesde;
	private JComboBox<LocalTime> comboBoxHasta;
	private JTextField textFieldDni;
	private JButton btnGenerarReciboDe;
	private JTextField txtInserteDni;
	private JButton btnOk;
	private JButton btnReserva;
	private JPanel panelOperacion;
	private JPanel panelReservar;
	private JComboBox<Instalacion> comboBoxInstalaciones;
	private JPanel panelDecision;
	private JLabel lblOperacion;
	private JButton btnReservar;
	private JButton btnCancelarReserva;
	private JButton btnSocio;
	private JButton btnAdministrador;
	private JPanel panelCard;
	private JLabel lblTotal;
	private JTextField txtTotal;
	private JButton btnAtras;
	private JButton btnGenerarCuotas;	
	private JButton btnOk2;
	private JLabel lblGente;
	private JTextField txtDni2;
	private JLabel lblFechaSeleccionada;
	private JComboBox<LocalDate> comboBoxFecha;
	private JButton btnHoraLlegada;
	private JButton btnHoraSalida;
	private JPanel panelReservaActividadMonitor;
	private JPanel panelActividades;
	private JLabel lblIndicarAsistencia;
	private JRadioButton rdbtnReservaDeInstalaciones;
	private JRadioButton rdbtnReservaDeActividades;
	private JLabel lblActividades;
	private JButton btnAñadirActividad;
	private JLabel lblInformacinDeLa;
	private JPanel panel_1;
	private JScrollPane sPReservas;
	private JList<String> listReservas;
	private JLabel label;
	private JPanel panel_2;
	private JLabel lblContable;
	private JPanel panelAñadirGenteDeCola;
	private JScrollPane sPActividades;
	private JList<String> listActividades;
	private JScrollPane sPPersonas;
	private JList<String> listPersonas;
	private JButton btnOk3;
	private JPanel panelAvisos;
	private JButton btnContinuar;
	private JLabel lblNombre;
	private JLabel lblHorario;
	private JLabel lblOperaciones;
	private JTextField textFieldNombre;
	private JTextField textFieldHorario;
	private JButton btnApuntarseAActividad;
	private JButton btnDesapuntarse;
	private JLabel lblOperacionesDeAdministracion;
	private JButton btnEliminarActividad;
	private JPanel panelOperacionesAdmin;
	private JPanel panelCardOperacionesAdmin;
	private JPanel panelAñadirActividad;
	private JLabel lblFechaInicial;
	private JLabel lblNDeSemanas;
	private JTextField textFieldSemanas;
	private JLabel lblId;
	private JButton btnAceptare;
	private JButton buttonSemanaAnterior;
	private JButton buttonSemanaSiguiente;
	private JPanel panelListaActividades;
	private JScrollPane scrollPaneActividades;
	private JLabel lblSeleccioneLaActividad;
	private JButton btnContinuarLista;
	private JList<Reserva> listaActividades;
	private JButton btnPasarListaA;
	private JButton btnMonitor;
	private JPanel panelDatePicker;
	private JLabel lblDisponibilidad;
	private JLabel llblVerCalendario;
	private JPanel panelMonitor;
	private JPanel panelAccionesMonitor;
	private JPanel panelBotonesMonitor;
	private JButton btnVerLista;
	private JLabel txtPlazas;
	private JLabel lblMonitor;
	private JTextField txtMonitor;
	private JButton btnAvisos;
	private JLabel lblDesdeAct;
	private JLabel lblHastaAct;
	private JComboBox<LocalTime> comboBoxDesdeAct;
	private JComboBox<LocalTime> comboBoxHastaAct;	
	private JComboBox<Actividad> comboBoxActividades;
	private JList<LocalDate> listFecha;
	private JLabel lblPlazasOfrecidas;
	private JTextField textFieldPlazas;
	private JRadioButton rdbtnMonitor;
	private JLabel lblAccionesDisponibles;
	private JPanel panelVerHoras;
	private JButton btnActualizarHoras;
	private JScrollPane scrollPaneLista;
	private JLabel labelPresionaParaModificar;
	private JList<Lista> listLista;
	private JButton btnReservarEventos;
	private JPanel panelReservarEventos;
	private JLabel lblDatosReserva;
	private JLabel lblNombreEvento;
	private JLabel lblSemanas;
	private JTextField textFieldNumSemanas;
	private JTextField textFieldEvento;
	private JButton btnReservarEvento;
	
	private LocalDateTime fechaSeleccionada;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					centro = new Centro();
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Métodos de lógica o generación dinámica de componentes

	private void creaBotonesOcupacionSemana(JPanel panel, Instalacion seleccionada, LocalDate fecha, boolean calendarioInstalaciones) {		

		panel.removeAll();
		lblDisponibilidad.setText(String.format("Fechas mostradas: Del %s hasta el %s", getDateFromDatePicker().toString(), getDateFromDatePicker().plusDays(6).toString()));

		LocalDateTime aComparar = LocalDateTime.of(fecha, LocalTime.of(0, 0));

		List<Reserva> reservas = null;
		if (calendarioInstalaciones) 							reservas = centro.getBaseDatos().getReservaEntreFechasPorInstalacion(seleccionada.getCodInstalacion(), fecha, fecha.plusDays(6));			
		else 									    	        reservas = centro.getBaseDatos().getReservaActividadesEntreFechasPorInstalacion(seleccionada.getCodInstalacion(), fecha, fecha.plusDays(6));	
					
		for (int k = 0; k < 7; k++) {			

			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.BOTH;
			c.gridx = k;						

			for (int i = -1; i < 24; i++) {					
				c.gridy = i + 1;
				JButton boton = null;

				if (i == -1) 
					if (calendarioInstalaciones)				 boton = creaSeleccionFechaInstalaciones(aComparar.toLocalDate(), comboBoxFecha);
					else								   		 boton = creaSeleccionFechaActividades(aComparar.toLocalDate(), getListFecha());					
						
				else {
					boton = creaBotonOcupacionLibre(aComparar);

					//Comprobamos si esta reservada esa hora				
					for (int j = 0; j < reservas.size(); j++) {

						if (!reservas.get(j).getInicioReserva().isAfter(aComparar)) {	

							Reserva reserva = reservas.get(j);
							LocalDateTime horaInicio = reserva.getInicioReserva();
							LocalDateTime horaFin = reserva.getFinReserva();

							if (aComparar.equals(horaInicio) || aComparar.equals(horaFin.minusMinutes(59)) || (aComparar.isAfter(horaInicio) && aComparar.isBefore(horaFin)))
								if (calendarioInstalaciones)	 modificaBotonOcupadoInstalaciones(boton, reserva);	
								else  	 						 modificaBotonOcupadoActividades(boton, reserva);
								
						}
					}
					aComparar = aComparar.plusHours(1);
				}				
				panel.add(boton, c);			
			}			
		}
		getContentPane().validate();
		getContentPane().repaint();
	}
	
	private void creaBotonesOcupacionMonitor(JPanel panel, LocalDate fecha) {		

		panel.removeAll();
		lblDisponibilidad.setText(String.format("Fechas mostradas: Del %s hasta el %s", getDateFromDatePicker().toString(), getDateFromDatePicker().plusDays(6).toString()));

		LocalDateTime aComparar = LocalDateTime.of(fecha, LocalTime.of(0, 0));

		List<Reserva> reservas = centro.getBaseDatos().getReservasMonitorEntreFechas(textFieldDni.getText(),fecha, fecha.plusDays(6));
				
		for (int k = 0; k < 7; k++) {			

			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.BOTH;
			c.gridx = k;						

			for (int i = -1; i < 24; i++) {					
				c.gridy = i + 1;
				JButton boton = null;

				if (i == -1) {	
					boton = new JButton(aComparar.toLocalDate().toString());
					boton.setName("btn" + aComparar.toLocalDate().toString());		
					boton.setBackground(Color.BLACK);
					boton.setForeground(Color.WHITE);		
				}
						
				else {
					boton = creaBotonOcupacionLibre(aComparar);

					//Comprobamos si esta reservada esa hora				
					for (int j = 0; j < reservas.size(); j++) {

						if (!reservas.get(j).getInicioReserva().isAfter(aComparar)) {	

							Reserva reserva = reservas.get(j);
							LocalDateTime horaInicio = reserva.getInicioReserva();
							LocalDateTime horaFin = reserva.getFinReserva();

							if (aComparar.equals(horaInicio) || aComparar.equals(horaFin.minusMinutes(59)) || (aComparar.isAfter(horaInicio) && aComparar.isBefore(horaFin)))
								modificaBotonOcupadoMonitor(boton, reserva);								
						}
					}
					aComparar = aComparar.plusHours(1);
				}				
				panel.add(boton, c);			
			}						
		}
		getContentPane().validate();
		getContentPane().repaint();
	}

	private JButton creaSeleccionFechaInstalaciones(final LocalDate fechaBoton, final JComboBox<LocalDate> combo) {

		final JButton boton = new JButton(fechaBoton.toString());		

		boton.setActionCommand(fechaBoton.toString());
		boton.setName("btn" + fechaBoton.toString());		
		boton.setBackground(Color.BLACK);
		boton.setForeground(Color.WHITE);		

		boton.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {				
			combo.setSelectedItem(fechaBoton);														
		}});

		return boton;
	}
	private JButton modificaBotonOcupadoInstalaciones(JButton boton, final Reserva reserva){

		boton.setActionCommand(reserva.getIdReserva());	

		if (centro.isAdmin()) //Si es administrador se pintan todas las reservas de socio de color rojo			
		{ 
			boton.setEnabled(true);

			if (!reserva.getTitular().equals("admin"))	boton.setBackground(Color.RED);
			else boton.setBackground(Color.ORANGE); //La reserva es de admin, se pinta naranja
		}
		else  //No es administrador
		{
			if (!reserva.getTitular().equals(textFieldDni.getText())) {
				boton.setBackground(Color.RED); //La reserva es de otro socio o de admin				
			}								
			else {
				boton.setBackground(Color.ORANGE);
				boton.setEnabled(true);//La reserva es suya, se pinta naranja		
			}
		}
		boton.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {	
			
			Socio socio = centro.getBaseDatos().getSocioConDni(reserva.getTitular());
			JOptionPane.showMessageDialog(null, reserva.mostrarDetallesParaInstalaciones(centro.isAdmin(), socio));
			comboBoxDesde.setSelectedItem(reserva.getInicioReserva().toLocalTime());
			comboBoxHasta.setSelectedItem(reserva.getFinReserva().toLocalTime());
		}});

		return boton;
	}

	private JButton modificaBotonOcupadoActividades(JButton boton, final Reserva reserva){

		boton.setActionCommand(reserva.getIdReserva());	
		boton.setEnabled(true);			
		boton.setBackground(Color.BLUE); 						

		boton.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {	
			
			fechaSeleccionada = reserva.getInicioReserva(); 
			
			if (centro.isAdmin()){
			int resp = JOptionPane.showConfirmDialog(null, reserva.mostrarDetallesParaActividades() + "\n ¿Desea cambiar el monitor?", "Datos reserva", JOptionPane.YES_NO_OPTION);
				if (resp == JOptionPane.YES_OPTION){
					String resp2 = JOptionPane.showInputDialog(null, "DNI del nuevo monitor:", "");
					for (Monitor m: centro.getMonitor()){
						if (m.getDni().equals(resp2)){
							if (!resp2.equals(null) && !comprobarMonitor(reserva.getInicioReserva(),reserva.getFinReserva(),resp2)){
								centro.getBaseDatos().updateMonitor(reserva.getIdReserva(),resp2); 
							}
							else {
								JOptionPane.showMessageDialog(null, b);
							}
						}
					} 
					JOptionPane.showMessageDialog(null, "Inserte un dni de monitor válido");
				}
			} else {
				
				JOptionPane.showMessageDialog(null, reserva.mostrarDetallesParaActividades());
			}
			textFieldNombre.setText(reserva.getTitular());
			textFieldHorario.setText(String.format("%s - %s", reserva.getInicioReserva().toLocalTime().toString(), reserva.getFinReserva().toLocalTime().toString()));			
		}});

		return boton;
	}
	
	private JButton modificaBotonOcupadoMonitor(JButton boton, final Reserva reserva){

		boton.setEnabled(true);			
		boton.setBackground(Color.BLUE);		

		boton.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {		

			JOptionPane.showMessageDialog(null, reserva.mostrarDetallesParaActividades());		
			
		}});

		return boton;
	}
	
	private JButton creaSeleccionFechaActividades(final LocalDate fechaBoton, final JList<LocalDate> lista) {

		final JButton boton = new JButton(fechaBoton.toString());		

		boton.setActionCommand(fechaBoton.toString());
		boton.setName("btn" + fechaBoton.toString());		
		boton.setBackground(Color.BLACK);
		boton.setForeground(Color.WHITE);		

		boton.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {	

			DefaultListModel<LocalDate> model = new DefaultListModel<LocalDate>();
			model.addElement(fechaBoton);			
			lista.setModel(model);
			lista.setSelectedIndex(0);															
		}});

		return boton;
	}

	private JButton creaBotonOcupacionLibre(LocalDateTime horaBoton) { 		

		JButton boton = new JButton(horaBoton.toLocalTime().toString());		

		boton.setActionCommand(horaBoton.toString());
		boton.setName("btn" + horaBoton.toString());		
		boton.setBackground(Color.GREEN);
		boton.setForeground(Color.BLACK);
		boton.setEnabled(false);

		return boton;
	}	

	public LocalDate getDateFromDatePicker() {		

		int year = datePicker.getModel().getYear();
		int month = datePicker.getModel().getMonth();
		int day = datePicker.getModel().getDay();	

		return LocalDate.of(year, month+1, day);
	}

	public void setDateIntoDatePicker(LocalDate fecha) {		

		datePicker.getModel().setYear(fecha.getYear());
		datePicker.getModel().setMonth(fecha.getMonthValue() - 1);
		datePicker.getModel().setDay(fecha.getDayOfMonth());	
	}

	public LocalDateTime getFechaCompletaSeleccionada(JComboBox<LocalTime> horas) {		
		return LocalDateTime.of((LocalDate) comboBoxFecha.getSelectedItem(), (LocalTime) horas.getSelectedItem());
	}	

	private List<LocalTime> getHorasDia() {

		List<LocalTime> horas = new ArrayList<LocalTime>();
		for (int i = 0; i < 24; i++) {
			horas.add(LocalTime.of(i, 0));
		}
		return horas;
	}


	//Métodos de componentes visibles

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaPrincipal() throws SQLException {


		model = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		datePanel = new JDatePanelImpl(model, p);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(100, 100, 990, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{45, 794, 40, 115, 100, 150, 90, 0};
		gbl_contentPane.rowHeights = new int[]{40, 15, 300, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		GridBagConstraints gbc_lblDni = new GridBagConstraints();
		gbc_lblDni.fill = GridBagConstraints.VERTICAL;
		gbc_lblDni.insets = new Insets(0, 0, 5, 5);
		gbc_lblDni.gridx = 2;
		gbc_lblDni.gridy = 0;
		contentPane.add(getLblID(), gbc_lblDni);
		GridBagConstraints gbc_textFieldDni = new GridBagConstraints();
		gbc_textFieldDni.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldDni.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldDni.gridx = 3;
		gbc_textFieldDni.gridy = 0;
		contentPane.add(getTextFieldDni(), gbc_textFieldDni);
		GridBagConstraints gbc_btnMonitor = new GridBagConstraints();
		gbc_btnMonitor.anchor = GridBagConstraints.EAST;
		gbc_btnMonitor.insets = new Insets(0, 0, 5, 5);
		gbc_btnMonitor.gridx = 4;
		gbc_btnMonitor.gridy = 0;
		contentPane.add(getBtnMonitor(), gbc_btnMonitor);
		GridBagConstraints gbc_btnAdministrador = new GridBagConstraints();
		gbc_btnAdministrador.insets = new Insets(0, 0, 5, 5);
		gbc_btnAdministrador.gridx = 5;
		gbc_btnAdministrador.gridy = 0;
		contentPane.add(getBtnAdministrador(), gbc_btnAdministrador);
		GridBagConstraints gbc_btnSocio = new GridBagConstraints();
		gbc_btnSocio.anchor = GridBagConstraints.WEST;
		gbc_btnSocio.insets = new Insets(0, 0, 5, 0);
		gbc_btnSocio.gridx = 6;
		gbc_btnSocio.gridy = 0;
		contentPane.add(getBtnSocio(), gbc_btnSocio);
		GridBagConstraints gbc_panelCard = new GridBagConstraints();
		gbc_panelCard.gridwidth = 7;
		gbc_panelCard.fill = GridBagConstraints.BOTH;
		gbc_panelCard.gridx = 0;
		gbc_panelCard.gridy = 2;
		contentPane.add(getPanelCard(), gbc_panelCard);
	}	
	private JPanel getPanelListInstalaciones() {
		if (panelListInstalaciones == null) {
			panelListInstalaciones = new JPanel();
			GridBagLayout gbl_panelListInstalaciones = new GridBagLayout();
			gbl_panelListInstalaciones.columnWidths = new int[]{200, 20, 100, 100, 100, 0, 0};
			gbl_panelListInstalaciones.rowHeights = new int[]{35, 35, 0};
			gbl_panelListInstalaciones.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panelListInstalaciones.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			panelListInstalaciones.setLayout(gbl_panelListInstalaciones);
			GridBagConstraints gbc_lblInstalacionesDisponibles = new GridBagConstraints();
			gbc_lblInstalacionesDisponibles.anchor = GridBagConstraints.WEST;
			gbc_lblInstalacionesDisponibles.insets = new Insets(0, 0, 5, 5);
			gbc_lblInstalacionesDisponibles.gridx = 0;
			gbc_lblInstalacionesDisponibles.gridy = 0;
			panelListInstalaciones.add(getLblInstalacionesDisponibles(), gbc_lblInstalacionesDisponibles);
			GridBagConstraints gbc_comboBoxInstalaciones = new GridBagConstraints();
			gbc_comboBoxInstalaciones.gridwidth = 3;
			gbc_comboBoxInstalaciones.insets = new Insets(0, 0, 5, 5);
			gbc_comboBoxInstalaciones.anchor = GridBagConstraints.WEST;
			gbc_comboBoxInstalaciones.gridx = 2;
			gbc_comboBoxInstalaciones.gridy = 0;
			panelListInstalaciones.add(getComboInstalaciones(), gbc_comboBoxInstalaciones);
			GridBagConstraints gbc_rdbtnMonitor = new GridBagConstraints();
			gbc_rdbtnMonitor.insets = new Insets(0, 0, 5, 0);
			gbc_rdbtnMonitor.gridx = 5;
			gbc_rdbtnMonitor.gridy = 0;
			panelListInstalaciones.add(getRdbtnMonitor(), gbc_rdbtnMonitor);
			GridBagConstraints gbc_llblVerCalendario = new GridBagConstraints();
			gbc_llblVerCalendario.anchor = GridBagConstraints.WEST;
			gbc_llblVerCalendario.insets = new Insets(0, 0, 0, 5);
			gbc_llblVerCalendario.gridx = 0;
			gbc_llblVerCalendario.gridy = 1;
			panelListInstalaciones.add(getLlblVerCalendario(), gbc_llblVerCalendario);
			GridBagConstraints gbc_rdbtnReservaDeInstalaciones = new GridBagConstraints();
			gbc_rdbtnReservaDeInstalaciones.insets = new Insets(0, 0, 0, 5);
			gbc_rdbtnReservaDeInstalaciones.gridx = 2;
			gbc_rdbtnReservaDeInstalaciones.gridy = 1;
			panelListInstalaciones.add(getRdbtnReservaDeInstalaciones(), gbc_rdbtnReservaDeInstalaciones);
			GridBagConstraints gbc_rdbtnReservaDeActividades = new GridBagConstraints();
			gbc_rdbtnReservaDeActividades.insets = new Insets(0, 0, 0, 5);
			gbc_rdbtnReservaDeActividades.gridx = 3;
			gbc_rdbtnReservaDeActividades.gridy = 1;
			panelListInstalaciones.add(getRdbtnReservaDeActividades(), gbc_rdbtnReservaDeActividades);
			GridBagConstraints gbc_btnContinuar = new GridBagConstraints();
			gbc_btnContinuar.insets = new Insets(0, 0, 0, 5);
			gbc_btnContinuar.gridx = 4;
			gbc_btnContinuar.gridy = 1;
			panelListInstalaciones.add(getBtnContinuar(), gbc_btnContinuar);
		}
		return panelListInstalaciones;
	}
	private JPanel getPanelInstalacionFecha() {
		if (panelInstalacionFecha == null) {
			panelInstalacionFecha = new JPanel();
			GridBagLayout gbl_panelInstalacionFecha = new GridBagLayout();
			gbl_panelInstalacionFecha.columnWidths = new int[]{45, 340, 30, 0, 130, 130, 45};
			gbl_panelInstalacionFecha.rowHeights = new int[]{40, 40, 25, 39, 25, 47, 0};
			gbl_panelInstalacionFecha.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0};
			gbl_panelInstalacionFecha.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelInstalacionFecha.setLayout(gbl_panelInstalacionFecha);
			GridBagConstraints gbc_panelListInstalaciones = new GridBagConstraints();
			gbc_panelListInstalaciones.gridwidth = 5;
			gbc_panelListInstalaciones.gridheight = 2;
			gbc_panelListInstalaciones.fill = GridBagConstraints.HORIZONTAL;
			gbc_panelListInstalaciones.insets = new Insets(0, 0, 5, 5);
			gbc_panelListInstalaciones.gridx = 1;
			gbc_panelListInstalaciones.gridy = 0;
			panelInstalacionFecha.add(getPanelListInstalaciones(), gbc_panelListInstalaciones);
			GridBagConstraints gbc_panel_1 = new GridBagConstraints();
			gbc_panel_1.gridwidth = 5;
			gbc_panel_1.insets = new Insets(0, 0, 5, 5);
			gbc_panel_1.fill = GridBagConstraints.BOTH;
			gbc_panel_1.gridx = 1;
			gbc_panel_1.gridy = 3;
			panelInstalacionFecha.add(getPanel_1(), gbc_panel_1);
			GridBagConstraints gbc_panel_2 = new GridBagConstraints();
			gbc_panel_2.insets = new Insets(0, 0, 0, 5);
			gbc_panel_2.fill = GridBagConstraints.HORIZONTAL;
			gbc_panel_2.gridx = 1;
			gbc_panel_2.gridy = 5;
			panelInstalacionFecha.add(getPanel_2(), gbc_panel_2);
			GridBagConstraints gbc_panelAvisos = new GridBagConstraints();
			gbc_panelAvisos.gridwidth = 2;
			gbc_panelAvisos.insets = new Insets(0, 0, 0, 5);
			gbc_panelAvisos.gridx = 4;
			gbc_panelAvisos.gridy = 5;
			panelInstalacionFecha.add(getPanelAvisos(), gbc_panelAvisos);
		}
		return panelInstalacionFecha;
	}
	private JLabel getLblInstalacionesDisponibles() {
		if (lblInstalacionesDisponibles == null) {
			lblInstalacionesDisponibles = new JLabel("Instalaciones disponibles:");
			lblInstalacionesDisponibles.setBorder(new EmptyBorder(4, 4, 4, 4));
			lblInstalacionesDisponibles.setHorizontalAlignment(SwingConstants.CENTER);
			lblInstalacionesDisponibles.setFont(new Font("Calibri", Font.BOLD, 16));
		}
		return lblInstalacionesDisponibles;
	}
	private JPanel getPanelHoraReserva() {
		if (panelHoraReserva == null) {
			panelHoraReserva = new JPanel();
			GridBagLayout gbl_panelHoraReserva = new GridBagLayout();
			gbl_panelHoraReserva.columnWidths = new int[]{45, 0, 430, 0, 30, 250, 45};
			gbl_panelHoraReserva.rowHeights = new int[]{20, 45, 200, 45, 0};
			gbl_panelHoraReserva.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0};
			gbl_panelHoraReserva.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};

			panelHoraReserva.setLayout(gbl_panelHoraReserva);
			GridBagConstraints gbc_lblDisponibilidad = new GridBagConstraints();
			gbc_lblDisponibilidad.gridwidth = 3;
			gbc_lblDisponibilidad.anchor = GridBagConstraints.SOUTH;
			gbc_lblDisponibilidad.insets = new Insets(0, 0, 5, 5);
			gbc_lblDisponibilidad.gridx = 1;
			gbc_lblDisponibilidad.gridy = 0;
			panelHoraReserva.add(getLblDisponibilidad(), gbc_lblDisponibilidad);
			GridBagConstraints gbc_buttonSemanaAnterior = new GridBagConstraints();
			gbc_buttonSemanaAnterior.insets = new Insets(0, 0, 5, 5);
			gbc_buttonSemanaAnterior.gridx = 1;
			gbc_buttonSemanaAnterior.gridy = 1;
			panelHoraReserva.add(getButtonSemanaAnterior(), gbc_buttonSemanaAnterior);
			GridBagConstraints gbc_panelDatePicker = new GridBagConstraints();
			gbc_panelDatePicker.insets = new Insets(0, 0, 5, 5);
			gbc_panelDatePicker.fill = GridBagConstraints.HORIZONTAL;
			gbc_panelDatePicker.gridx = 2;
			gbc_panelDatePicker.gridy = 1;
			panelHoraReserva.add(getPanelDatePicker(), gbc_panelDatePicker);
			GridBagConstraints gbc_buttonSemanaSiguiente = new GridBagConstraints();
			gbc_buttonSemanaSiguiente.insets = new Insets(0, 0, 5, 5);
			gbc_buttonSemanaSiguiente.gridx = 3;
			gbc_buttonSemanaSiguiente.gridy = 1;
			panelHoraReserva.add(getButtonSemanaSiguiente(), gbc_buttonSemanaSiguiente);
			GridBagConstraints gbc_panelReservaActividad = new GridBagConstraints();
			gbc_panelReservaActividad.gridheight = 3;
			gbc_panelReservaActividad.insets = new Insets(0, 0, 5, 5);
			gbc_panelReservaActividad.fill = GridBagConstraints.BOTH;
			gbc_panelReservaActividad.gridx = 5;
			gbc_panelReservaActividad.gridy = 0;
			panelHoraReserva.add(getPanelReservaActividadMonitor(), gbc_panelReservaActividad);
			GridBagConstraints gbc_panelHoras = new GridBagConstraints();
			gbc_panelHoras.gridwidth = 3;
			gbc_panelHoras.insets = new Insets(0, 0, 5, 5);
			gbc_panelHoras.fill = GridBagConstraints.BOTH;
			gbc_panelHoras.gridx = 1;
			gbc_panelHoras.gridy = 2;
			panelHoraReserva.add(getPanelHoras(), gbc_panelHoras);
			GridBagConstraints gbc_btnAtras = new GridBagConstraints();
			gbc_btnAtras.anchor = GridBagConstraints.EAST;
			gbc_btnAtras.insets = new Insets(0, 0, 0, 5);
			gbc_btnAtras.gridx = 5;
			gbc_btnAtras.gridy = 3;
			panelHoraReserva.add(getBtnAtras(), gbc_btnAtras);
		}
		return panelHoraReserva;
	}
	private JPanel getPanelHoras() {
		if (panelHoras == null) {
			panelHoras = new JPanel();
			GridBagLayout gbl_panelHoras = new GridBagLayout();
			gbl_panelHoras.columnWidths = new int[]{43, 0};
			gbl_panelHoras.rowHeights = new int[] {276, 0};
			gbl_panelHoras.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panelHoras.rowWeights = new double[]{1.0, 0.0};
			panelHoras.setLayout(gbl_panelHoras);
			GridBagConstraints gbc_scrollPaneHoras = new GridBagConstraints();
			gbc_scrollPaneHoras.insets = new Insets(0, 0, 5, 0);
			gbc_scrollPaneHoras.fill = GridBagConstraints.BOTH;
			gbc_scrollPaneHoras.gridx = 0;
			gbc_scrollPaneHoras.gridy = 0;
			panelHoras.add(getScrollPaneHoras(), gbc_scrollPaneHoras);
			GridBagConstraints gbc_txtrColorNaranja = new GridBagConstraints();
			gbc_txtrColorNaranja.fill = GridBagConstraints.BOTH;
			gbc_txtrColorNaranja.gridx = 0;
			gbc_txtrColorNaranja.gridy = 1;
			panelHoras.add(getTxtLeyenda(), gbc_txtrColorNaranja);
		}
		return panelHoras;
	}
	private JScrollPane getScrollPaneHoras() {
		if (scrollPaneHoras == null) {
			scrollPaneHoras = new JScrollPane();
			scrollPaneHoras.setBorder(new LineBorder(Color.BLACK));
			scrollPaneHoras.setViewportView(getPanelVerHoras());
		}
		return scrollPaneHoras;
	}
	private JTextArea getTxtLeyenda() {
		if (txtLeyenda == null) {
			txtLeyenda = new JTextArea();
			txtLeyenda.setBorder(new LineBorder(new Color(0, 0, 0)));
			txtLeyenda.setRows(2);
			txtLeyenda.setEditable(false);
			txtLeyenda.setFont(new Font("Tahoma", Font.PLAIN, 12));
			txtLeyenda.setText("- Color Verde: Libre\r\n- Color Rojo: Reservado por Socio\r\n- Color Naranja: Reservado por Centro");
		}
		return txtLeyenda;
	}
	private JPanel getPanelReservas() {
		if (panelReservas == null) {
			panelReservas = new JPanel();
			panelReservas.setBackground(UIManager.getColor("Panel.background"));
			GridBagLayout gbl_panelReservas = new GridBagLayout();
			gbl_panelReservas.columnWidths = new int[] {72, 126, 110};
			gbl_panelReservas.rowHeights = new int[] {35, 45, 45, 45, 10, 161};
			gbl_panelReservas.columnWeights = new double[]{0.0, 0.0, 1.0};
			gbl_panelReservas.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0};
			panelReservas.setLayout(gbl_panelReservas);
			GridBagConstraints gbc_lblIntroduzcaHorasDeseadas = new GridBagConstraints();
			gbc_lblIntroduzcaHorasDeseadas.anchor = GridBagConstraints.NORTH;
			gbc_lblIntroduzcaHorasDeseadas.gridwidth = 3;
			gbc_lblIntroduzcaHorasDeseadas.insets = new Insets(0, 0, 5, 5);
			gbc_lblIntroduzcaHorasDeseadas.gridx = 0;
			gbc_lblIntroduzcaHorasDeseadas.gridy = 0;
			panelReservas.add(getLblIntroduzcaHorasDeseadas(), gbc_lblIntroduzcaHorasDeseadas);
			GridBagConstraints gbc_lblFechaSeleccionada = new GridBagConstraints();
			gbc_lblFechaSeleccionada.insets = new Insets(0, 0, 5, 5);
			gbc_lblFechaSeleccionada.gridx = 0;
			gbc_lblFechaSeleccionada.gridy = 1;
			panelReservas.add(getLblFechaSeleccionada(), gbc_lblFechaSeleccionada);
			GridBagConstraints gbc_comboBoxFecha = new GridBagConstraints();
			gbc_comboBoxFecha.gridwidth = 2;
			gbc_comboBoxFecha.insets = new Insets(0, 0, 5, 0);
			gbc_comboBoxFecha.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBoxFecha.gridx = 1;
			gbc_comboBoxFecha.gridy = 1;
			panelReservas.add(getComboBoxFecha(), gbc_comboBoxFecha);
			GridBagConstraints gbc_lblDesde = new GridBagConstraints();
			gbc_lblDesde.insets = new Insets(0, 0, 5, 5);
			gbc_lblDesde.gridx = 0;
			gbc_lblDesde.gridy = 2;
			panelReservas.add(getLblDesde(), gbc_lblDesde);
			GridBagConstraints gbc_comboBoxDesde = new GridBagConstraints();
			gbc_comboBoxDesde.gridwidth = 2;
			gbc_comboBoxDesde.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBoxDesde.insets = new Insets(0, 0, 5, 0);
			gbc_comboBoxDesde.gridx = 1;
			gbc_comboBoxDesde.gridy = 2;
			panelReservas.add(getComboBoxDesde(), gbc_comboBoxDesde);
			GridBagConstraints gbc_lblHasta = new GridBagConstraints();
			gbc_lblHasta.insets = new Insets(0, 0, 5, 5);
			gbc_lblHasta.gridx = 0;
			gbc_lblHasta.gridy = 3;
			panelReservas.add(getLblHasta(), gbc_lblHasta);
			GridBagConstraints gbc_comboBoxHasta = new GridBagConstraints();
			gbc_comboBoxHasta.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBoxHasta.gridwidth = 2;
			gbc_comboBoxHasta.insets = new Insets(0, 0, 5, 0);
			gbc_comboBoxHasta.gridx = 1;
			gbc_comboBoxHasta.gridy = 3;
			panelReservas.add(getComboBoxHasta(), gbc_comboBoxHasta);
			GridBagConstraints gbc_panelOperacion = new GridBagConstraints();
			gbc_panelOperacion.gridwidth = 3;
			gbc_panelOperacion.fill = GridBagConstraints.BOTH;
			gbc_panelOperacion.gridx = 0;
			gbc_panelOperacion.gridy = 5;
			panelReservas.add(getPanelOperacion(), gbc_panelOperacion);
		}
		return panelReservas;
	}
	private JLabel getLblIntroduzcaHorasDeseadas() {
		if (lblIntroduzcaHorasDeseadas == null) {
			lblIntroduzcaHorasDeseadas = new JLabel("Informaci\u00F3n de la reserva");
			lblIntroduzcaHorasDeseadas.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblIntroduzcaHorasDeseadas;
	}
	private JLabel getLblDesde() {
		if (lblDesde == null) {
			lblDesde = new JLabel("Desde:");			
		}
		return lblDesde;
	}
	private JLabel getLblHasta() {
		if (lblHasta == null) {
			lblHasta = new JLabel("Hasta:");			
		}
		return lblHasta;
	}
	private JLabel getLblInformacionPersonal() {
		if (lblInformacionPersonal == null) {
			lblInformacionPersonal = new JLabel("Informaci\u00F3n personal:");
			lblInformacionPersonal.setFont(new Font("Calibri", Font.BOLD, 18));
			lblInformacionPersonal.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblInformacionPersonal;
	}
	private JLabel getLblID() {
		if (lblID == null) {
			lblID = new JLabel("ID:");
			lblID.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return lblID;
	}
	private JLabel getLblFormaDePago() {
		if (lblFormaDePago == null) {
			lblFormaDePago = new JLabel("Forma de Pago:");
			lblFormaDePago.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return lblFormaDePago;
	}
	private JRadioButton getRdbtnEfectivo() {
		if (rdbtnEfectivo == null) {
			rdbtnEfectivo = new JRadioButton("Efectivo");
			rdbtnEfectivo.setEnabled(false);
			rdbtnEfectivo.setSelected(true);
			buttonGroup.add(rdbtnEfectivo);
			rdbtnEfectivo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return rdbtnEfectivo;
	}
	private JRadioButton getRdbtnCuota() {
		if (rdbtnCuota == null) {
			rdbtnCuota = new JRadioButton("Cuota");
			buttonGroup.add(rdbtnCuota);
			rdbtnCuota.setFont(new Font("Tahoma", Font.PLAIN, 12));			
		}
		return rdbtnCuota;
	}
	private JComboBox<LocalTime> getComboBoxDesde() {

		if (comboBoxDesde == null) {	

			DefaultComboBoxModel<LocalTime> horas = new DefaultComboBoxModel<LocalTime>();
			for (LocalTime i : getHorasDia())
				horas.addElement(i);

			comboBoxDesde = new JComboBox<LocalTime>(horas);			
		}
		return comboBoxDesde;
	}
	private JComboBox<LocalTime> getComboBoxHasta() {

		if (comboBoxHasta == null) {

			DefaultComboBoxModel<LocalTime> horas = new DefaultComboBoxModel<LocalTime>();
			for (LocalTime i : getHorasDia())
				horas.addElement(i.plusMinutes(59));

			comboBoxHasta = new JComboBox<LocalTime>(horas);			
		}
		return comboBoxHasta;
	}
	private JTextField getTextFieldDni() {
		if (textFieldDni == null) {
			textFieldDni = new JTextField();
			textFieldDni.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldDni.setColumns(10);
		}
		return textFieldDni;
	}
	private JButton getBtnGenerarReciboDe() {
		if (btnGenerarReciboDe == null) {
			btnGenerarReciboDe = new JButton("Generar recibo de pago en efectivo");
			btnGenerarReciboDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtInserteDni.setEditable(true);
					txtInserteDni.setEnabled(true);
				}
			});
		}
		return btnGenerarReciboDe;
	}
	private JTextField getTxtInserteDni() {
		if (txtInserteDni == null) {
			txtInserteDni = new JTextField();
			txtInserteDni.setEnabled(false);
			txtInserteDni.setEditable(false);
			txtInserteDni.setText("Inserte DNI");
			txtInserteDni.setColumns(10);
		}
		return txtInserteDni;
	}

	public void preTicket(){
		String dni = txtInserteDni.getText();
		DefaultListModel<String> reservas = new DefaultListModel<String>();
		for (Reserva c: centro.getReservas()){
			if (c.getTitular().equals(dni) && !c.isPagado()){
				reservas.addElement(c.getIdReserva());
			} 
		}
		listReservas.setModel(reservas);
	}


	public void ticket() throws IOException, SQLException{
		String aux = (String) listReservas.getSelectedValue();
		BufferedWriter fichero;

		for (Reserva r: centro.getReservas()){			


			String aux2 =(r.getIdReserva());
			if(aux2.equals(aux)  && !r.isPagado()){	
				r.setPagado(true);			
				centro.getBaseDatos().setPagado(r);
				String nombreFichero = "files/ticket-" + r.getTitular() + ".dat";
				fichero = new BufferedWriter(new FileWriter(nombreFichero));
				String linea = "";
				fichero.write(linea);
				fichero.newLine();
				String aux3= (""+r.getInicioReserva());
				String aux4= (""+r.getFinReserva());
				fichero.write("Reservante: " + r.getTitular() +" "+ "Inicio de reserva: " + aux3 + " Fin de reserva: " + aux4);
				fichero.newLine();
				String auxIns= "";
				for(Instalacion i : centro.getInstalaciones()){
					if (i.getCodInstalacion().equals(r.getCodInstalacion())){
						auxIns = i.getNombre();
					}
				}
				fichero.write("instalación: " + auxIns);
				fichero.newLine();
				fichero.write("Precio: " + r.getPrecio());
				fichero.close();

			}
		}
	}



	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton("OK");
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					String dni = txtInserteDni.getText();					
					List<Reserva> reservas;					
					boolean aux = true;
					reservas = centro.getReservas();
					for (Reserva r: reservas){
						if (dni.equals(r.getTitular())){
							preTicket();
							sPReservas.setEnabled(true);
							aux = false;
						} 
					} if (aux) JOptionPane.showMessageDialog(btnOk, "Este DNI no tiene reservas");
				}
			});
		}
		return btnOk;
	}

	private JButton getBtnReserva() {
		
		if (btnReserva == null) {			
			btnReserva = new JButton("Reservar");          
			btnReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {										

					//Datos que necesito guardar en la reserva	
					Reserva rs;
					float precio = Float.parseFloat(txtTotal.getText());
					String dni = textFieldDni.getText();
					String codInstalacion = ((Instalacion) comboBoxInstalaciones.getSelectedItem()).getCodInstalacion();
					LocalDateTime fechaInicio = getFechaCompletaSeleccionada(getComboBoxDesde());
					LocalDateTime fechaFin = getFechaCompletaSeleccionada(getComboBoxHasta());

					Reserva colision = Comprobaciones.getReservaEnColision(fechaInicio, fechaFin, codInstalacion);				
					boolean añadir = true;
					
					if (colision != null) 
						añadir = añadirNuevaReservaEnCasoDeColision(colision)[0];
					
					if (añadir) {		
						
						if (!dni.equals("admin"))  rs = new Reserva(codInstalacion, dni, fechaInicio, fechaFin, null, null, precio, false, rdbtnCuota.isSelected(),null,0);					
						else   					   rs = new Reserva(codInstalacion, dni, fechaInicio, fechaFin, null, null, precio, true, false,null,0);
						
						JOptionPane.showMessageDialog(null, rs.mostrarDetallesParaInstalaciones(centro.isAdmin(), centro.getBaseDatos().getSocioConDni(dni)));
						centro.getBaseDatos().guardarReserva(rs);
						creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());
					}
				}
			});
		}
		return btnReserva;
	}

	private boolean[] añadirNuevaReservaEnCasoDeColision(Reserva colision) {

		boolean añadir = true;	
		boolean cancelar = false;
		String error = "Existe una colisión de horario e instalación con la reserva:\n\n" + colision.mostrarDetallesParaInstalaciones(centro.isAdmin(), null);

		if (centro.isAdmin()) {	
			
			int option = JOptionPane.showConfirmDialog(null, error + "\n\n¿Desea cancelar la reserva existente y generar la nueva reserva?");
			if (option == JOptionPane.YES_OPTION) 
				JOptionPane.showMessageDialog(null, centro.getBaseDatos().cancelarReserva(colision));	
			
			else if (option == JOptionPane.CANCEL_OPTION)
				cancelar = true;
			
			else añadir = false;					
		}
		else {
			JOptionPane.showMessageDialog(null, error);
			añadir = false; //No se puede añadir la nueva reserva
		}
		return new boolean[]{añadir, cancelar};
	}

	private JPanel getPanelOperacion() {
		if (panelOperacion == null) {
			panelOperacion = new JPanel();
			panelOperacion.setLayout(new CardLayout(0, 0));
			panelOperacion.add(getPanelDecision(), "panelDecision");
			panelOperacion.add(getPanelReservar(), "panelReservar");
			panelOperacion.add(getPanelReservarEventos(), "panelReservarEventos");
		}
		return panelOperacion;
	}
	private JPanel getPanelReservar() {
		if (panelReservar == null) {
			panelReservar = new JPanel();
			GridBagLayout gbl_panelReservar = new GridBagLayout();
			gbl_panelReservar.columnWidths = new int[]{100, 100, 100, 0};
			gbl_panelReservar.rowHeights = new int[]{40, 40, 40, 0};
			gbl_panelReservar.columnWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
			gbl_panelReservar.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelReservar.setLayout(gbl_panelReservar);
			GridBagConstraints gbc_lblInformacinPersonal = new GridBagConstraints();
			gbc_lblInformacinPersonal.fill = GridBagConstraints.VERTICAL;
			gbc_lblInformacinPersonal.gridwidth = 3;
			gbc_lblInformacinPersonal.insets = new Insets(0, 0, 5, 0);
			gbc_lblInformacinPersonal.gridx = 0;
			gbc_lblInformacinPersonal.gridy = 0;
			panelReservar.add(getLblInformacionPersonal(), gbc_lblInformacinPersonal);
			GridBagConstraints gbc_lblFormaDePago = new GridBagConstraints();
			gbc_lblFormaDePago.insets = new Insets(0, 0, 5, 5);
			gbc_lblFormaDePago.gridx = 0;
			gbc_lblFormaDePago.gridy = 1;
			panelReservar.add(getLblFormaDePago(), gbc_lblFormaDePago);
			GridBagConstraints gbc_rdbtnEfectivo = new GridBagConstraints();
			gbc_rdbtnEfectivo.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnEfectivo.gridx = 1;
			gbc_rdbtnEfectivo.gridy = 1;
			panelReservar.add(getRdbtnEfectivo(), gbc_rdbtnEfectivo);
			GridBagConstraints gbc_rdbtnCuota = new GridBagConstraints();
			gbc_rdbtnCuota.insets = new Insets(0, 0, 5, 0);
			gbc_rdbtnCuota.gridx = 2;
			gbc_rdbtnCuota.gridy = 1;
			panelReservar.add(getRdbtnCuota(), gbc_rdbtnCuota);
			GridBagConstraints gbc_lblTotal = new GridBagConstraints();
			gbc_lblTotal.insets = new Insets(0, 0, 0, 5);
			gbc_lblTotal.gridx = 0;
			gbc_lblTotal.gridy = 2;
			panelReservar.add(getLblTotal(), gbc_lblTotal);
			GridBagConstraints gbc_txtTotal = new GridBagConstraints();
			gbc_txtTotal.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtTotal.insets = new Insets(0, 0, 0, 5);
			gbc_txtTotal.gridx = 1;
			gbc_txtTotal.gridy = 2;
			panelReservar.add(getTxtTotal(), gbc_txtTotal);
			GridBagConstraints gbc_btnOK = new GridBagConstraints();
			gbc_btnOK.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnOK.gridx = 2;
			gbc_btnOK.gridy = 2;
			panelReservar.add(getBtnReserva(), gbc_btnOK);
		}
		return panelReservar;
	}
	private JComboBox<Instalacion> getComboInstalaciones() {

		if (comboBoxInstalaciones == null) {
			comboBoxInstalaciones = new JComboBox<Instalacion>();			
			comboBoxInstalaciones.setName("Instalaciones disponibles");
			comboBoxInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 12));		 

			List<Instalacion> instalaciones = centro.getInstalaciones();
			for (Instalacion i : instalaciones)
				comboBoxInstalaciones.addItem(i);				
		}
		return comboBoxInstalaciones;
	}
	private JPanel getPanelDecision() {
		if (panelDecision == null) {
			panelDecision = new JPanel();
			GridBagLayout gbl_panelDecision = new GridBagLayout();
			gbl_panelDecision.columnWidths = new int[]{140, 15, 140, 0};
			gbl_panelDecision.rowHeights = new int[]{40, 40, 40, 10, 40, 40, 0};
			gbl_panelDecision.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
			gbl_panelDecision.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelDecision.setLayout(gbl_panelDecision);
			GridBagConstraints gbc_lblOperacion = new GridBagConstraints();
			gbc_lblOperacion.insets = new Insets(0, 0, 5, 0);
			gbc_lblOperacion.gridwidth = 3;
			gbc_lblOperacion.gridx = 0;
			gbc_lblOperacion.gridy = 0;
			panelDecision.add(getLblOperacion(), gbc_lblOperacion);
			GridBagConstraints gbc_btnReservar = new GridBagConstraints();
			gbc_btnReservar.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnReservar.insets = new Insets(0, 0, 5, 5);
			gbc_btnReservar.gridx = 0;
			gbc_btnReservar.gridy = 1;
			panelDecision.add(getBtnReservar(), gbc_btnReservar);
			GridBagConstraints gbc_btnCancelarReserva = new GridBagConstraints();
			gbc_btnCancelarReserva.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnCancelarReserva.insets = new Insets(0, 0, 5, 0);
			gbc_btnCancelarReserva.gridx = 2;
			gbc_btnCancelarReserva.gridy = 1;
			panelDecision.add(getBtnCancelarReserva(), gbc_btnCancelarReserva);
			GridBagConstraints gbc_btnReservarEventos = new GridBagConstraints();
			gbc_btnReservarEventos.gridwidth = 3;
			gbc_btnReservarEventos.insets = new Insets(0, 0, 5, 0);
			gbc_btnReservarEventos.gridx = 0;
			gbc_btnReservarEventos.gridy = 2;
			panelDecision.add(getBtnReservarEventos(), gbc_btnReservarEventos);
			GridBagConstraints gbc_lblIndicarAsistencia = new GridBagConstraints();
			gbc_lblIndicarAsistencia.gridwidth = 3;
			gbc_lblIndicarAsistencia.insets = new Insets(0, 0, 5, 0);
			gbc_lblIndicarAsistencia.gridx = 0;
			gbc_lblIndicarAsistencia.gridy = 4;
			panelDecision.add(getLblIndicarAsistencia(), gbc_lblIndicarAsistencia);
			GridBagConstraints gbc_btnHoraLlegada = new GridBagConstraints();
			gbc_btnHoraLlegada.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnHoraLlegada.insets = new Insets(0, 0, 0, 5);
			gbc_btnHoraLlegada.gridx = 0;
			gbc_btnHoraLlegada.gridy = 5;
			panelDecision.add(getBtnHoraLlegada(), gbc_btnHoraLlegada);
			GridBagConstraints gbc_btnHoraSalida = new GridBagConstraints();
			gbc_btnHoraSalida.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnHoraSalida.gridx = 2;
			gbc_btnHoraSalida.gridy = 5;
			panelDecision.add(getBtnHoraSalida(), gbc_btnHoraSalida);
		}
		return panelDecision;
	}
	private JLabel getLblOperacion() {
		if (lblOperacion == null) {
			lblOperacion = new JLabel("Operaci\u00F3n a realizar");
			lblOperacion.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblOperacion;
	}

	private JButton getBtnReservar() {
		if (btnReservar == null) {
			btnReservar = new JButton("Reservar");			
			btnReservar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					LocalDateTime fechaInicioSeleccionada = getFechaCompletaSeleccionada(getComboBoxDesde());
					LocalDateTime fechaFinSeleccionada = getFechaCompletaSeleccionada(getComboBoxHasta());

					String error = Comprobaciones.comprobarFechasValidas(fechaInicioSeleccionada, fechaFinSeleccionada, textFieldDni.getText(), centro.isAdmin());
					error += Comprobaciones.comprobarDniSocio(textFieldDni.getText(), centro.getBaseDatos());	
					if (!error.equals("")) 
						JOptionPane.showMessageDialog(null, error);
					else 
					{						
						if (!textFieldDni.getText().equals("admin")) {
							Float precio = ((Instalacion) comboBoxInstalaciones.getSelectedItem()).getPrecio()* Comprobaciones.calcularNumeroDeHorasReservadas(fechaInicioSeleccionada, fechaFinSeleccionada);
							txtTotal.setText(precio.toString());

							//Habilito opciones que aplican a socio
							rdbtnCuota.setEnabled(true);
							rdbtnEfectivo.setEnabled(true);						
						}
						else {
							txtTotal.setText("0");			
							//Deshabilito opciones que no aplican a administración
							rdbtnCuota.setEnabled(false);
							rdbtnEfectivo.setEnabled(false);							
						}
						//Cambiamos la card del Panel
						CardLayout cardLayout = (CardLayout) panelOperacion.getLayout();
						cardLayout.next(panelOperacion);	
					}				
				}
			});			
		}
		return btnReservar;
	}
	private JButton getBtnCancelarReserva() {
		if (btnCancelarReserva == null) {
			btnCancelarReserva = new JButton("Cancelar Reserva");					

			btnCancelarReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					String dni = textFieldDni.getText();

					LocalDateTime fechaCompletaInicio = getFechaCompletaSeleccionada(getComboBoxDesde());
					LocalDateTime fechaCompletaFin = getFechaCompletaSeleccionada(getComboBoxHasta());

					String error = Comprobaciones.comprobarDniSocio(textFieldDni.getText(), centro.getBaseDatos());						
					error += Comprobaciones.comprobarFechasValidas(fechaCompletaInicio, fechaCompletaFin, textFieldDni.getText(), centro.isAdmin());										

					if(!error.equals("")){
						JOptionPane.showMessageDialog(null, error);
					}
					else {			

						String nombreInstalacion = ((Instalacion) comboBoxInstalaciones.getSelectedItem()).getNombre();
						String cod = "";
						float precio = 0;


						List<Instalacion> instalaciones = centro.getInstalaciones();

						for (int i = 0; i < instalaciones.size(); i++) {
							if(instalaciones.get(i).getNombre().equals(nombreInstalacion)){
								Instalacion ins = instalaciones.get(i);
								cod = instalaciones.get(i).getCodInstalacion();
								precio =  instalaciones.get(i).getPrecio();
								ins.setDisponible(true);
							}
						}						
						Reserva reserva = new Reserva(cod, dni, fechaCompletaInicio, fechaCompletaFin, null, null,precio, false, rdbtnCuota.isSelected(), null, 0) ;

						try {

							String mensaje = centro.getBaseDatos().cancelarReserva(reserva);

							if(centro.isAdmin()){
								if(isReservaSocio(reserva.getTitular())){
									//La administracion cancela la reserva de un socio

									String motivo = JOptionPane.showInputDialog("Causa de la cancelación: ");
									centro.getBaseDatos().insertReservasCanceladas(reserva, true, motivo);
								}
								else{
									//El centro cancela una reserva del centro

									String motivo = "Reserva cancelada por el centro";
									centro.getBaseDatos().insertReservasCanceladas(reserva, false, motivo);
								}
							}
							else{
								//El socio cancela una reserva propia

								String motivo = "Reserva cancelada por el socio";
								centro.getBaseDatos().insertReservasCanceladas(reserva, false, motivo);
							}

							JOptionPane.showMessageDialog(null, mensaje);

							creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());

						} catch (SQLException e) {
							e.printStackTrace();
						}								
					}
				}
			});
		}
		return btnCancelarReserva;
	}

	public boolean isReservaSocio(String id){
		if(!id.equals("admin")){
			List<Socio> socios = centro.getBaseDatos().getSocios();
			for(Socio s : socios){
				if(s.getDni().equals(id)){
					return true;
				}
			}
		}
		return false;

	}

	private JButton getBtnSocio() {
		if (btnSocio == null) {
			btnSocio = new JButton("Socio");			
			btnSocio.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					centro.setAdmin(false);

					btnSocio.setBackground(Color.RED);					
					btnAdministrador.setBackground(null);
					btnMonitor.setBackground(null);

					//Deshabilito los botones únicos de administración
					btnHoraLlegada.setVisible(false);
					btnHoraSalida.setVisible(false);	
					lblIndicarAsistencia.setVisible(false);
					getLblOperacionesDeAdministracion().setVisible(false);
					getPanelCardOperacionesAdmin().setVisible(false);
					panel_1.setVisible(false);
					panel_2.setVisible(false);
					panelAñadirGenteDeCola.setVisible(false);
					panelAvisos.setVisible(false);

					//Muestro el panel correspondiente
					CardLayout cardLayout = (CardLayout) panelCard.getLayout();
					cardLayout.show(getPanelCard(), "panelInstalacionFecha");
				}
			});
		} 
		return btnSocio;
	}
	private JButton getBtnAdministrador() {
		if (btnAdministrador == null) {
			btnAdministrador = new JButton("Administraci\u00F3n");
			btnAdministrador.setBackground(Color.RED);			

			btnAdministrador.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					centro.setAdmin(true);	

					btnAdministrador.setBackground(Color.RED);
					btnSocio.setBackground(null);	
					btnMonitor.setBackground(null);

					//Habilito los botones únicos de administración
					btnHoraLlegada.setVisible(true);
					btnHoraSalida.setVisible(true);	
					lblIndicarAsistencia.setVisible(true);
					getLblOperacionesDeAdministracion().setVisible(true);
					getPanelCardOperacionesAdmin().setVisible(true);
					panel_1.setVisible(true);	
					panel_2.setVisible(true);			
					panelAñadirGenteDeCola.setVisible(true);
					panelAvisos.setVisible(true);

					//Muestro el panel correspondiente
					CardLayout cardLayout = (CardLayout) panelCard.getLayout();
					cardLayout.show(getPanelCard(), "panelInstalacionFecha");
				}
			});
		}
		return btnAdministrador;
	}	
	private JButton getBtnMonitor() {
		if (btnMonitor == null) {
			btnMonitor = new JButton("Monitor");
			btnMonitor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					centro.setAdmin(false);

					btnMonitor.setBackground(Color.RED);					
					btnAdministrador.setBackground(null);
					btnSocio.setBackground(null);
					rdbtnMonitor.setSelected(true);
					
					txtLeyenda.setText("- Color Azul: Horas con actividades impartidas por ti\n- Color Verde: Horas libres\n");
					creaBotonesOcupacionMonitor(panelVerHoras, getDateFromDatePicker());

					//Muestro el panel correspondiente
					//Se establece el card de las Actividades
					CardLayout cardLayout = (CardLayout) getPanelReservaActividadMonitor().getLayout();
					cardLayout.show(getPanelReservaActividadMonitor(), "panelMonitor");							

					//Se a la siguiente ventana
					cardLayout = (CardLayout) panelCard.getLayout();
					cardLayout.show(getPanelCard(), "panelHoraReserva");
				}
			});
		}
		return btnMonitor;
	}
	private JPanel getPanelCard() {
		if (panelCard == null) {
			panelCard = new JPanel();
			panelCard.setLayout(new CardLayout(0, 0));
			panelCard.add(getPanelInstalacionFecha(), "panelInstalacionFecha");
			panelCard.add(getPanelHoraReserva(), "panelHoraReserva");
		}
		return panelCard;
	}
	private JLabel getLblTotal() {
		if (lblTotal == null) {
			lblTotal = new JLabel("Total:");
			lblTotal.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return lblTotal;
	}
	private JTextField getTxtTotal() {
		if (txtTotal == null) {
			txtTotal = new JTextField();
			txtTotal.setHorizontalAlignment(SwingConstants.CENTER);
			txtTotal.setText("0");
			txtTotal.setFont(new Font("Tahoma", Font.PLAIN, 12));
			txtTotal.setEditable(false);
			txtTotal.setColumns(10);



		}
		return txtTotal;
	}
	private JButton getBtnAtras() {
		if (btnAtras == null) {
			btnAtras = new JButton("Atr\u00E1s");
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {	

					btnSocio.setEnabled(true);
					btnAdministrador.setEnabled(true);
					btnMonitor.setEnabled(true);
					textFieldDni.setEditable(true);
					rdbtnReservaDeInstalaciones.setSelected(true);
					scrollPaneLista.setVisible(false);

					CardLayout cardLayout = (CardLayout) panelOperacion.getLayout();
					cardLayout.first(panelOperacion);	
					
					cardLayout = (CardLayout) panelCard.getLayout();
					cardLayout.first(panelCard);		
					
					cardLayout = (CardLayout) panelCardOperacionesAdmin.getLayout();
					cardLayout.first(panelCardOperacionesAdmin);
					
					cardLayout = (CardLayout) panelAccionesMonitor.getLayout();
					cardLayout.first(panelAccionesMonitor);
					//rellenaAvisos();
				}
			});			
		}
		return btnAtras;
	}
	private JButton getBtnGenerarCuotas() {
		if (btnGenerarCuotas == null) {
			btnGenerarCuotas = new JButton("Generar cuotas");
			btnGenerarCuotas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					List<Socio> socios = centro.getSocios();
					List <Reserva> reservas = centro.getReservas();
					float cantidad = 0;
					for (Socio c: socios){
						String dni= c.getDni();
						for (Reserva r: reservas){
							if (r.getTitular().equals(dni) && !r.isPagado())
								try {
									{
										cantidad = r.getPrecio();
										LocalDateTime fecha = r.getInicioReserva();
										try {
											centro.getBaseDatos().updateCuota(dni,cantidad,calcularMes(fecha),fecha,r.getIdReserva());
											centro.getBaseDatos().updateCuotaTotal();
										} catch (SQLException e) {											
											e.printStackTrace();
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
						}

					}
				}
			});
		}
		return btnGenerarCuotas;
	}

	public String getMonthName(int mes){
		String aux="";
		if (mes == 1){
			aux= "Enero";
		}
		if (mes == 2){
			aux= "Febrero";
		}
		if (mes == 3){
			aux= "Marzo";
		}
		if (mes == 4){
			aux= "Abril";
		}
		if (mes == 5){
			aux= "Mayo";
		}
		if (mes == 6){
			aux= "Junio";
		}
		if (mes == 7){
			aux= "Julio";
		}
		if (mes == 8){
			aux= "Agosto";
		}
		if (mes == 9){
			aux= "Septiembre";
		}
		if (mes == 10){
			aux= "Octubre";
		}
		if (mes == 11){
			aux= "Noviembre";
		}
		if (mes == 12){
			aux= "Diciembre";
		}
		return aux;

	}

	public String calcularMes(LocalDateTime fecha){
		int mes = fecha.getMonthValue();
		int dia = fecha.getDayOfMonth();
		if (dia<=19){
			mes = mes+1;
		}
		return getMonthName(mes);
	}

	protected void preAct() {
		DefaultListModel<String> actividades = new DefaultListModel<String>();
		for (Reserva r : centro.getReservas()) {
			for (Actividad a : centro.getActividades()) {
				if (r.getTitular().equals(a.getIdActividad())) {
					int dia = r.getInicioReserva().getDayOfMonth();
					int mes = r.getInicioReserva().getMonthValue();
					int diaActual = LocalDateTime.now().getDayOfMonth();
					int mesActual = LocalDateTime.now().getMonthValue();
					if ((dia == diaActual) && (mes == mesActual) && r.getNumPlazas() != 99999) {
						actividades.addElement(r.getTitular() + "-" + r.getInicioReserva().getHour() + ":00-"
								+ nombreInstalacion(r.getCodInstalacion()));
					}
				}
			}
		}
		listActividades.setModel(actividades);

	}

	public String nombreInstalacion(String cod){
		for (Instalacion i: centro.getInstalaciones()){
			if (i.getCodInstalacion().equals(cod)){
				return i.getNombre();
			}
		} return "";
	}

	public String codigoInstalacion(String cod){
		for (Instalacion i: centro.getInstalaciones()){
			if (i.getNombre().equals(cod)){
				return i.getCodInstalacion();
			}
		} return "";
	}

	protected void prePersonas() throws SQLException {
		personas.clear();
		String seleccionada =  (String) listActividades.getSelectedValue();
		String[] reserv = seleccionada.split("-");
		String idReserva="";
		for (Reserva r: centro.getReservas()){
			if ((r.getTitular().equals(reserv[0])) && ((r.getInicioReserva().getHour()+":00").equals(reserv[1]))
					&& (r.getCodInstalacion().equals(codigoInstalacion(reserv[2])))){
				idReserva = r.getIdReserva();
			}
		}
		String aux="no";
		for (Lista c: centro.getBaseDatos().getListas()){
			if (c.isAsistencia()){ aux="si";}
			if (idReserva.equals(c.getIdReserva()))
				personas.addElement(c.getDni()+ " asistencia: " + aux);

		} listPersonas.setModel(personas);
	}

	private JButton getBtnOk2() {
		if (btnOk2 == null) {
			btnOk2 = new JButton("ok");
			btnOk2.setVisible(false);
			btnOk2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						prePersonas();
					} catch (SQLException e) {					
						e.printStackTrace();
					}
					lblGente.setVisible(true);
					btnOk3.setVisible(true);
					txtDni2.setVisible(true);
					txtPlazas.setVisible(true);
					try {
						plazasLibres();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			});
		}
		return btnOk2;
	}

	protected void plazasLibres() throws SQLException {	
		String seleccionada =  (String) listActividades.getSelectedValue();
		String[] reserv = seleccionada.split("-");		
		for (Reserva r: centro.getReservas()){
			if ((r.getTitular().equals(reserv[0])) && ((r.getInicioReserva().getHour()+":00").equals(reserv[1]))
					&& (r.getCodInstalacion().equals(codigoInstalacion(reserv[2])))){
				int total = r.getNumPlazas()-(centro.getPlazasOcupadas(r.getIdReserva())); 
				this.total = total;
				txtPlazas.setText("Quedan "+ total + " plazas libres"); 		
			} }
	}


	private JLabel getLblGente() {
		if (lblGente == null) {
			lblGente = new JLabel("A\u00F1adir gente a la cola:");
			lblGente.setVisible(false);
		}
		return lblGente;
	}
	private JTextField getTxtDni2() {
		if (txtDni2 == null) {
			txtDni2 = new JTextField();
			txtDni2.setVisible(false);
			txtDni2.setText("Inserte DNI");
			txtDni2.setColumns(10);
		}
		return txtDni2;
	}
	private JLabel getLblFechaSeleccionada() {
		if (lblFechaSeleccionada == null) {
			lblFechaSeleccionada = new JLabel("Fecha:");			
		}
		return lblFechaSeleccionada;
	}
	private JComboBox<LocalDate> getComboBoxFecha() {
		if (comboBoxFecha == null) {	
			comboBoxFecha = new JComboBox<LocalDate>();		


		}
		return comboBoxFecha;
	}

	private DefaultComboBoxModel<LocalDate> rellenaComboBoxFecha() {

		DefaultComboBoxModel<LocalDate> fechas = new DefaultComboBoxModel<LocalDate>();
		LocalDate fecha = getDateFromDatePicker();
		int i = 0;
		do {
			fechas.addElement(fecha);
			fecha = fecha.plusDays(1);
			i++;
		} while (i < 7);

		return fechas;
	}
	private JButton getBtnHoraLlegada() {
		if (btnHoraLlegada == null) {
			btnHoraLlegada = new JButton("Hora de Llegada");
			btnHoraLlegada.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {					 
					try {
						List<Reserva> reservas = centro.getBaseDatos().getReservasPorTitular(textFieldDni.getText());
						for(int i = 0; i< reservas.size();i++){
							if(reservas.get(i).getInicioReserva().equals(getFechaCompletaSeleccionada(getComboBoxDesde()))
									&& reservas.get(i).getFinReserva().equals(getFechaCompletaSeleccionada((getComboBoxHasta())))
									&& reservas.get(i).getHoraEntrada()==null){							
							}
							reservas.get(i).setHoraEntrada(LocalDateTime.now());
							centro.getBaseDatos().updateHoraEntrada(reservas.get(i));							
							if(!reservas.get(i).getHoraEntrada().equals(null))
								JOptionPane.showMessageDialog(null, "Hora insertada con exito");
						}
					} catch (SQLException e1) {						
						e1.printStackTrace();
					}			
				}
			});
		}
		return btnHoraLlegada;

	}
	private JButton getBtnHoraSalida() {
		if (btnHoraSalida == null) {
			btnHoraSalida = new JButton("Hora de Salida");
			btnHoraSalida.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {						
					try {
						List<Reserva> reservas = centro.getBaseDatos().getReservasPorTitular(textFieldDni.getText());
						for(int i = 0; i< reservas.size();i++){
							if(reservas.get(i).getInicioReserva().equals(getFechaCompletaSeleccionada(getComboBoxDesde()))
									&& reservas.get(i).getFinReserva().equals(getFechaCompletaSeleccionada((getComboBoxHasta())))
									&& reservas.get(i).getHoraSalida()==null){							
							}
							reservas.get(i).setHoraSalida(LocalDateTime.now());
							centro.getBaseDatos().updateHoraSalida(reservas.get(i));							
							if(!reservas.get(i).getHoraSalida().equals(null))
								JOptionPane.showMessageDialog(null, "Hora insertada con exito");
						}
					} catch (SQLException e1) {					
						e1.printStackTrace();
					}
				}
			});
		}
		return btnHoraSalida;
	}
	private JPanel getPanelReservaActividadMonitor() {
		if (panelReservaActividadMonitor == null) {
			panelReservaActividadMonitor = new JPanel();
			panelReservaActividadMonitor.setLayout(new CardLayout(0, 0));
			panelReservaActividadMonitor.add(getPanelReservas(), "panelReservas");
			panelReservaActividadMonitor.add(getPanelActividades(), "panelActividades");
			panelReservaActividadMonitor.add(getPanelMonitor(), "panelMonitor");
		}
		return panelReservaActividadMonitor;
	}
	private JPanel getPanelActividades() {
		if (panelActividades == null) {
			panelActividades = new JPanel();
			GridBagLayout gbl_panelActividades = new GridBagLayout();
			gbl_panelActividades.columnWidths = new int[]{99, 145};
			gbl_panelActividades.rowHeights = new int[]{35, 45, 45, 45, 45, 45, 45, 0, 0};
			gbl_panelActividades.columnWeights = new double[]{0.0, 1.0};
			gbl_panelActividades.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			panelActividades.setLayout(gbl_panelActividades);
			GridBagConstraints gbc_lblInformacinDeLa = new GridBagConstraints();
			gbc_lblInformacinDeLa.anchor = GridBagConstraints.NORTH;
			gbc_lblInformacinDeLa.gridwidth = 2;
			gbc_lblInformacinDeLa.insets = new Insets(0, 0, 5, 0);
			gbc_lblInformacinDeLa.gridx = 0;
			gbc_lblInformacinDeLa.gridy = 0;
			panelActividades.add(getLblInformacinDeLa(), gbc_lblInformacinDeLa);
			GridBagConstraints gbc_lblNombre = new GridBagConstraints();
			gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
			gbc_lblNombre.gridx = 0;
			gbc_lblNombre.gridy = 1;
			panelActividades.add(getLblNombre(), gbc_lblNombre);
			GridBagConstraints gbc_textFieldNombre = new GridBagConstraints();
			gbc_textFieldNombre.insets = new Insets(0, 0, 5, 0);
			gbc_textFieldNombre.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldNombre.gridx = 1;
			gbc_textFieldNombre.gridy = 1;
			panelActividades.add(getTextFieldNombre(), gbc_textFieldNombre);
			GridBagConstraints gbc_lblHorario = new GridBagConstraints();
			gbc_lblHorario.insets = new Insets(0, 0, 5, 5);
			gbc_lblHorario.gridx = 0;
			gbc_lblHorario.gridy = 2;
			panelActividades.add(getLblHorario(), gbc_lblHorario);
			GridBagConstraints gbc_textFieldHorario = new GridBagConstraints();
			gbc_textFieldHorario.insets = new Insets(0, 0, 5, 0);
			gbc_textFieldHorario.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldHorario.gridx = 1;
			gbc_textFieldHorario.gridy = 2;
			panelActividades.add(getTextFieldHorario(), gbc_textFieldHorario);
			GridBagConstraints gbc_lblOperaciones = new GridBagConstraints();
			gbc_lblOperaciones.insets = new Insets(0, 0, 5, 0);
			gbc_lblOperaciones.gridwidth = 2;
			gbc_lblOperaciones.gridx = 0;
			gbc_lblOperaciones.gridy = 3;
			panelActividades.add(getLblOperaciones(), gbc_lblOperaciones);
			GridBagConstraints gbc_btnApuntarseAActividad = new GridBagConstraints();
			gbc_btnApuntarseAActividad.gridwidth = 2;
			gbc_btnApuntarseAActividad.insets = new Insets(0, 0, 5, 0);
			gbc_btnApuntarseAActividad.gridx = 0;
			gbc_btnApuntarseAActividad.gridy = 4;
			panelActividades.add(getBtnApuntarseAActividad(), gbc_btnApuntarseAActividad);
			GridBagConstraints gbc_btnDesapuntarse = new GridBagConstraints();
			gbc_btnDesapuntarse.gridwidth = 2;
			gbc_btnDesapuntarse.insets = new Insets(0, 0, 5, 0);
			gbc_btnDesapuntarse.gridx = 0;
			gbc_btnDesapuntarse.gridy = 5;
			panelActividades.add(getBtnDesapuntarse(), gbc_btnDesapuntarse);
			GridBagConstraints gbc_lblOperacionesDeAdministracion = new GridBagConstraints();
			gbc_lblOperacionesDeAdministracion.gridwidth = 2;
			gbc_lblOperacionesDeAdministracion.insets = new Insets(0, 0, 5, 0);
			gbc_lblOperacionesDeAdministracion.gridx = 0;
			gbc_lblOperacionesDeAdministracion.gridy = 6;
			panelActividades.add(getLblOperacionesDeAdministracion(), gbc_lblOperacionesDeAdministracion);
			GridBagConstraints gbc_panelCardOperacionesAdmin = new GridBagConstraints();
			gbc_panelCardOperacionesAdmin.gridwidth = 2;
			gbc_panelCardOperacionesAdmin.fill = GridBagConstraints.BOTH;
			gbc_panelCardOperacionesAdmin.gridx = 0;
			gbc_panelCardOperacionesAdmin.gridy = 7;
			panelActividades.add(getPanelCardOperacionesAdmin(), gbc_panelCardOperacionesAdmin);
		}
		return panelActividades;
	}
	private JLabel getLblIndicarAsistencia() {
		if (lblIndicarAsistencia == null) {
			lblIndicarAsistencia = new JLabel("Indicar asistencia de socio");
			lblIndicarAsistencia.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblIndicarAsistencia;
	}
	private JRadioButton getRdbtnReservaDeInstalaciones() {
		if (rdbtnReservaDeInstalaciones == null) {
			rdbtnReservaDeInstalaciones = new JRadioButton("Instalaciones");
			rdbtnReservaDeInstalaciones.setSelected(true);
			buttonGroup2.add(rdbtnReservaDeInstalaciones);
		}
		return rdbtnReservaDeInstalaciones;
	}
	private JRadioButton getRdbtnReservaDeActividades() {
		if (rdbtnReservaDeActividades == null) {
			rdbtnReservaDeActividades = new JRadioButton("Actividades");
			buttonGroup2.add(rdbtnReservaDeActividades);
		}
		return rdbtnReservaDeActividades;
	}
	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Actividades:");
			lblActividades.setFont(new Font("Tahoma", Font.BOLD, 12));
			lblActividades.setVisible(false);
			lblActividades.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return lblActividades;
	}
	private JButton getBtnAñadirActividad() {
		if (btnAñadirActividad == null) {
			btnAñadirActividad = new JButton("A\u00F1adir nueva actividad al calendario");
			btnAñadirActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					CardLayout card = (CardLayout) panelCardOperacionesAdmin.getLayout();
					card.show(panelCardOperacionesAdmin, "panelAñadirActividad");					
				}
			});
		}
		return btnAñadirActividad;
	}
	private JLabel getLblInformacinDeLa() {
		if (lblInformacinDeLa == null) {
			lblInformacinDeLa = new JLabel("Actividad seleccionada");
			lblInformacinDeLa.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblInformacinDeLa;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[]{42, 203, 0, 98, 103, 203, 0};
			gridBagLayout.rowHeights = new int[]{38, 38, 0};
			gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
			panel_1.setLayout(gridBagLayout);
			GridBagConstraints gbc_btnGenerarReciboDe = new GridBagConstraints();
			gbc_btnGenerarReciboDe.gridwidth = 3;
			gbc_btnGenerarReciboDe.insets = new Insets(0, 0, 5, 5);
			gbc_btnGenerarReciboDe.gridx = 1;
			gbc_btnGenerarReciboDe.gridy = 0;
			panel_1.add(getBtnGenerarReciboDe(), gbc_btnGenerarReciboDe);
			GridBagConstraints gbc_sPReservas = new GridBagConstraints();
			gbc_sPReservas.fill = GridBagConstraints.BOTH;
			gbc_sPReservas.gridheight = 2;
			gbc_sPReservas.gridx = 5;
			gbc_sPReservas.gridy = 0;
			panel_1.add(getSPReservas(), gbc_sPReservas);
			GridBagConstraints gbc_label = new GridBagConstraints();
			gbc_label.insets = new Insets(0, 0, 0, 5);
			gbc_label.anchor = GridBagConstraints.EAST;
			gbc_label.gridx = 0;
			gbc_label.gridy = 1;
			panel_1.add(getLabel(), gbc_label);
			GridBagConstraints gbc_txtInserteDni = new GridBagConstraints();
			gbc_txtInserteDni.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtInserteDni.gridwidth = 3;
			gbc_txtInserteDni.insets = new Insets(0, 0, 0, 5);
			gbc_txtInserteDni.gridx = 1;
			gbc_txtInserteDni.gridy = 1;
			panel_1.add(getTxtInserteDni(), gbc_txtInserteDni);
			GridBagConstraints gbc_btnOk = new GridBagConstraints();
			gbc_btnOk.insets = new Insets(0, 0, 0, 5);
			gbc_btnOk.gridx = 4;
			gbc_btnOk.gridy = 1;
			panel_1.add(getBtnOk(), gbc_btnOk);
		}
		return panel_1;
	}
	private JScrollPane getSPReservas() {
		if (sPReservas == null) {
			sPReservas = new JScrollPane();
			sPReservas.setViewportView(getListReservas());
		}
		return sPReservas;
	}
	private JList<String> getListReservas() {
		if (listReservas == null) {
			listReservas = new JList<String>();
		}
		return listReservas;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("");
		}
		return label;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GridBagLayout gbl_panel_2 = new GridBagLayout();
			gbl_panel_2.columnWidths = new int[]{64, 67, 144, 0};
			gbl_panel_2.rowHeights = new int[]{42, 0};
			gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			panel_2.setLayout(gbl_panel_2);
			GridBagConstraints gbc_lblContable = new GridBagConstraints();
			gbc_lblContable.gridwidth = 2;
			gbc_lblContable.insets = new Insets(0, 0, 0, 5);
			gbc_lblContable.gridx = 0;
			gbc_lblContable.gridy = 0;
			panel_2.add(getLblContable(), gbc_lblContable);
			GridBagConstraints gbc_btnGenerarCuotas = new GridBagConstraints();
			gbc_btnGenerarCuotas.gridx = 2;
			gbc_btnGenerarCuotas.gridy = 0;
			panel_2.add(getBtnGenerarCuotas(), gbc_btnGenerarCuotas);
		}
		return panel_2;
	}
	private JLabel getLblContable() {
		if (lblContable == null) {
			lblContable = new JLabel("Contable:");
			lblContable.setFont(new Font("Calibri", Font.BOLD, 16));
		}
		return lblContable;
	}
	private JPanel getPanelAñadirGenteDeCola() {
		if (panelAñadirGenteDeCola == null) {
			panelAñadirGenteDeCola = new JPanel();
			panelAñadirGenteDeCola.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			GridBagLayout gbl_panelAñadirGenteDeCola = new GridBagLayout();
			gbl_panelAñadirGenteDeCola.columnWidths = new int[]{35, 100, 2, 100, 35, 0};
			gbl_panelAñadirGenteDeCola.rowHeights = new int[]{35, 132, 32, 190, 0, 0, 0, 0};
			gbl_panelAñadirGenteDeCola.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
			gbl_panelAñadirGenteDeCola.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelAñadirGenteDeCola.setLayout(gbl_panelAñadirGenteDeCola);
			GridBagConstraints gbc_btnPasarLista = new GridBagConstraints();
			gbc_btnPasarLista.anchor = GridBagConstraints.NORTH;
			gbc_btnPasarLista.insets = new Insets(0, 0, 5, 5);
			gbc_btnPasarLista.gridx = 1;
			gbc_btnPasarLista.gridy = 1;
			GridBagConstraints gbc_lblActividades = new GridBagConstraints();
			gbc_lblActividades.insets = new Insets(0, 0, 5, 5);
			gbc_lblActividades.gridx = 1;
			gbc_lblActividades.gridy = 0;
			panelAñadirGenteDeCola.add(getLblActividades(), gbc_lblActividades);
			GridBagConstraints gbc_sPActividades = new GridBagConstraints();
			gbc_sPActividades.gridwidth = 3;
			gbc_sPActividades.insets = new Insets(0, 0, 5, 5);
			gbc_sPActividades.fill = GridBagConstraints.BOTH;
			gbc_sPActividades.gridx = 1;
			gbc_sPActividades.gridy = 1;
			panelAñadirGenteDeCola.add(getSPActividades(), gbc_sPActividades);
			GridBagConstraints gbc_btnOk2 = new GridBagConstraints();
			gbc_btnOk2.insets = new Insets(0, 0, 5, 5);
			gbc_btnOk2.gridx = 2;
			gbc_btnOk2.gridy = 2;
			panelAñadirGenteDeCola.add(getBtnOk2(), gbc_btnOk2);
			GridBagConstraints gbc_sPPersonas = new GridBagConstraints();
			gbc_sPPersonas.gridwidth = 3;
			gbc_sPPersonas.insets = new Insets(0, 0, 5, 5);
			gbc_sPPersonas.fill = GridBagConstraints.BOTH;
			gbc_sPPersonas.gridx = 1;
			gbc_sPPersonas.gridy = 3;
			panelAñadirGenteDeCola.add(getSPPersonas(), gbc_sPPersonas);
			GridBagConstraints gbc_txtPlazas = new GridBagConstraints();
			gbc_txtPlazas.gridwidth = 3;
			gbc_txtPlazas.insets = new Insets(0, 0, 5, 5);
			gbc_txtPlazas.gridx = 1;
			gbc_txtPlazas.gridy = 4;
			panelAñadirGenteDeCola.add(getTxtPlazas(), gbc_txtPlazas);
			GridBagConstraints gbc_lblGente = new GridBagConstraints();
			gbc_lblGente.gridwidth = 2;
			gbc_lblGente.insets = new Insets(0, 0, 5, 5);
			gbc_lblGente.gridx = 1;
			gbc_lblGente.gridy = 5;
			panelAñadirGenteDeCola.add(getLblGente(), gbc_lblGente);
			GridBagConstraints gbc_txtDni2 = new GridBagConstraints();
			gbc_txtDni2.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtDni2.insets = new Insets(0, 0, 5, 5);
			gbc_txtDni2.gridx = 3;
			gbc_txtDni2.gridy = 5;
			panelAñadirGenteDeCola.add(getTxtDni2(), gbc_txtDni2);
			GridBagConstraints gbc_btnOk3 = new GridBagConstraints();
			gbc_btnOk3.insets = new Insets(0, 0, 0, 5);
			gbc_btnOk3.gridx = 3;
			gbc_btnOk3.gridy = 6;
			panelAñadirGenteDeCola.add(getBtnOk3(), gbc_btnOk3);
		}
		return panelAñadirGenteDeCola;
	}
	private JScrollPane getSPActividades() {
		if (sPActividades == null) {
			sPActividades = new JScrollPane();
			sPActividades.setVisible(false);
			sPActividades.setViewportView(getListActividades());
		}
		return sPActividades;
	}
	private JList<String> getListActividades() {
		if (listActividades == null) {
			listActividades = new JList<String>();
		}
		return listActividades;
	}
	private JScrollPane getSPPersonas() {
		if (sPPersonas == null) {
			sPPersonas = new JScrollPane();
			sPPersonas.setVisible(false);
			sPPersonas.setViewportView(getListPersonas());
		}
		return sPPersonas;
	}
	private JList<String> getListPersonas() {
		if (listPersonas == null) {
			listPersonas = new JList<String>();
		}
		return listPersonas;
	}
	private JButton getBtnOk3() {
		if (btnOk3 == null) {
			btnOk3 = new JButton("ok");
			btnOk3.setVisible(false);
			btnOk3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					// aqui cojo datos de la reserva seleccionada
					String dni = txtDni2.getText();
					String id = "";
					LocalDateTime date = null;
					LocalDateTime date2 = null;
					String[] reserv = ((String) listActividades.getSelectedValue()).split("-");
					for (Reserva r : centro.getReservas()) {
						if ((r.getTitular().equals(reserv[0]))
								&& ((r.getInicioReserva().getHour() + ":00").equals(reserv[1]))
								&& (r.getCodInstalacion().equals(codigoInstalacion(reserv[2])))) {
							id = r.getIdReserva();
							date = r.getInicioReserva();
							date2 = r.getFinReserva();
						}
					}

					//para saber si es un socio
					boolean aux = false;
					for (Socio s : centro.getBaseDatos().getSocios()) {
						if (s.getDni().equals(dni)) {
							aux = true;
						}
					}

					try {
						for (Reserva reser : buscaReservasPorDni(dni)) {
							if (reser.getInicioReserva().equals(date)) {
								JOptionPane.showMessageDialog(btnOk3, "Ya tiene otra actividad a esta hora");
								return; }
							else if ((reser.getInicioReserva().isBefore(date)) && (reser.getFinReserva().isAfter(date)))	{
								JOptionPane.showMessageDialog(btnOk3, "Está apuntado a una actividad que estará en curso cuando ésta empiece");
								return;
							} else if ((reser.getInicioReserva().isBefore(date2)) && (reser.getFinReserva().isAfter(date2)))	{
								JOptionPane.showMessageDialog(btnOk3, "Está apuntado a una actividad que empezará mientras ésta esté en curso");
								return;
							}
						}	
						for (Reserva reser : getReservasInsDni(dni)) {
							if (reser.getInicioReserva().equals(date)) {
								JOptionPane.showMessageDialog(btnOk3, "Ya tiene una reserva de instalacion a esta hora");
								return; }
							else if ((reser.getInicioReserva().isBefore(date)) && (reser.getFinReserva().isAfter(date)))	{
								JOptionPane.showMessageDialog(btnOk3, "Tiene una reserva de instalación en curso");
								return;
							} else if ((reser.getInicioReserva().isBefore(date2)) && (reser.getFinReserva().isAfter(date2)))	{
								JOptionPane.showMessageDialog(btnOk3, "Tiene una reserva de instalación que empezará mientras ésta actividad esté en curso");
								return;
							}
						}
					}  catch (SQLException e1) {						
						e1.printStackTrace();
					}
					if (!aux) {
						JOptionPane.showMessageDialog(btnOk3, "Este dni no es de un socio");
						return;
					}
					if (total <= 0) {
						JOptionPane.showMessageDialog(btnOk3, "Las plazas estan llenas");
						return;
					}
					try {
						centro.getBaseDatos().metePersona(id, dni, true);
					} catch (SQLException e) {						
						e.printStackTrace();
					}

				}
			});
		}
		return btnOk3;
	}

	protected ArrayList<Reserva> getReservasInsDni(String dni) {
		ArrayList<Reserva> aux = new ArrayList<Reserva>();
		for (Reserva r: centro.getReservas()){
			if (r.getTitular().equals(dni)){
				aux.add(r);
			}
		}
		return aux;
	}

	public ArrayList<Reserva> buscaReservasPorDni(String dni) throws SQLException{
		String id="";
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		for (Lista l: centro.getLista()){
			if (dni.equals(l.getDni())){
				id = l.getIdReserva();
			}
			for (Reserva r: centro.getReservas()){
				if (r.getIdReserva().equals(id)){
					reservas.add(r);
				}

			}
		} return reservas;
	} 
	private JPanel getPanelAvisos() {
		if (panelAvisos == null) {
			panelAvisos = new JPanel();
			panelAvisos.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panelAvisos.setLayout(new BorderLayout(0, 0));
			panelAvisos.add(getBtnAvisos(), BorderLayout.NORTH);
		}
		return panelAvisos;
	}
	private JButton getBtnContinuar() {
		if (btnContinuar == null) {
			btnContinuar = new JButton("Continuar");			
			btnContinuar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					String error = "";			
					if (comboBoxInstalaciones.getSelectedIndex() < 0) 	error += "- Debe seleccionar una instalación\n";

					if(error.equals("")) {

						btnSocio.setEnabled(false);
						btnAdministrador.setEnabled(false);		
						btnMonitor.setEnabled(false);
						textFieldDni.setEditable(false);

						creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());

						if (getRdbtnReservaDeInstalaciones().isSelected()) {

							//Se cambia la leyenda
							if (centro.isAdmin())	txtLeyenda.setText("- Color Verde: Libre\n- Color Naranja: Reservado por Centro\n- Color Rojo: Reservado por Socio\n");
							else					txtLeyenda.setText("- Color Verde: Libre\n- Color Naranja: Reservado por ti\n- Color Rojo: Reservado por otro socio o el centro");

							comboBoxFecha.setModel(rellenaComboBoxFecha());								

							//Se establece el card de las Instalaciones
							CardLayout cardLayout = (CardLayout) panelReservaActividadMonitor.getLayout();
							cardLayout.show(panelReservaActividadMonitor, "panelReservas" );							
						} 
						else {

							txtLeyenda.setText("- Color Azul: Horas con actividades\n- Color Verde: Horas sin actividades\n");

							//Se establece el card de las Actividades
							CardLayout cardLayout = (CardLayout) getPanelReservaActividadMonitor().getLayout();
							cardLayout.show(getPanelReservaActividadMonitor(), "panelActividades");							
						}
						//Se a la siguiente ventana
						CardLayout cardLayout = (CardLayout) panelCard.getLayout();
						cardLayout.show(getPanelCard(), "panelHoraReserva");
					}
					else
						JOptionPane.showMessageDialog(null, error);
				}});
		}
		return btnContinuar;
	}
	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre:");
		}
		return lblNombre;
	}
	private JLabel getLblHorario() {
		if (lblHorario == null) {
			lblHorario = new JLabel("Horario:");
		}
		return lblHorario;
	}
	private JLabel getLblOperaciones() {
		if (lblOperaciones == null) {
			lblOperaciones = new JLabel("Operaciones a realizar");
			lblOperaciones.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblOperaciones;
	}
	private JTextField getTextFieldNombre() {
		if (textFieldNombre == null) {
			textFieldNombre = new JTextField();
			textFieldNombre.setText("Ninguna");
			textFieldNombre.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldNombre.setEditable(false);
			textFieldNombre.setColumns(10);
		}
		return textFieldNombre;
	}
	private JTextField getTextFieldHorario() {
		if (textFieldHorario == null) {
			textFieldHorario = new JTextField();
			textFieldHorario.setText("No aplica");
			textFieldHorario.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldHorario.setEditable(false);
			textFieldHorario.setColumns(10);
		}
		return textFieldHorario;
	}
	private JButton getBtnApuntarseAActividad() {
		if (btnApuntarseAActividad == null) {
			btnApuntarseAActividad = new JButton("Apuntarse a actividad");
			btnApuntarseAActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {				
					//Datos que necesito guardar
					Lista li = null;
					String dni = textFieldDni.getText();
					String idActividad = textFieldNombre.getText();							
					Reserva re = centro.getBaseDatos().getReservaActividad(idActividad);
					String error = "";					
					error = Comprobaciones.comprobarAntelacionActividades(re);
					error += Comprobaciones.comprobarNumeroPlazas(re);					
					if(error.equals("")){
						li = new Lista(re.getIdReserva(), dni, false);
						if(li.getDni()!= ""){
							centro.getBaseDatos().guardarReservaPlazaActividad(li);
							JOptionPane.showMessageDialog(null, "Ha sido apuntado");
						}
						else
							JOptionPane.showMessageDialog(null, "No ha sido apuntado");
					}
					else{
						JOptionPane.showMessageDialog(null, error);
					}				

				}

			});
		}
		return btnApuntarseAActividad;
	}
	private JButton getBtnDesapuntarse() {
		if (btnDesapuntarse == null) {
			btnDesapuntarse = new JButton("Desapuntarse a una actividad");
			btnDesapuntarse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					
//					String idActividad = textFieldNombre.getText();
//					String dni = textFieldDni.getText();
//					boolean asistencia = false;
//
//					Lista lista = new Lista(idActividad, dni, asistencia);
//
//					String mensaje = centro.getBaseDatos().cancelarAsistenciaActividad(lista);
//
//					JOptionPane.showMessageDialog(null, mensaje);
					
					LocalDateTime dateTimeInicio = fechaSeleccionada;

					
					String año = String.valueOf((int) fechaSeleccionada.getYear());
					String mes = String.valueOf(fechaSeleccionada.getMonth().getValue());
					String dia =  String.valueOf(fechaSeleccionada.getDayOfMonth());
					String hora = "";
					if(fechaSeleccionada.getHour() < 10){
						hora = "0" + String.valueOf(fechaSeleccionada.getHour());
					}
					else{
						hora = String.valueOf(fechaSeleccionada.getHour());
					}
					String minutos = "59";
					String segundos = String.valueOf(fechaSeleccionada.getSecond()+"0");

					
					
					String fechaHoraFin = año + "-" + mes + "-" + dia + " " + hora + ":" + minutos + ":" + segundos;

					DateTimeFormatter formatterFin = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					LocalDateTime dateTimeFin = LocalDateTime.parse(fechaHoraFin, formatterFin);

					
					
					String error = Comprobaciones.comprobarFechaHoraCancelacion(dateTimeInicio, dateTimeFin, textFieldDni.getText(), centro.isAdmin());										

					if(!error.equals("")){
						JOptionPane.showMessageDialog(null, error);
					}
					else {			

						String instalacion = String.valueOf(getComboInstalaciones().getSelectedIndex()+1);
						
					
						String idActividad =  String.valueOf(dateTimeInicio) + "-" + String.valueOf(dateTimeFin)
								+ "-" + textFieldNombre.getText() + "-" + instalacion;

					
						String dni = textFieldDni.getText();
						boolean asistencia = false;

						Lista lista = new Lista(idActividad, dni, asistencia);
				
						
						String mensaje = centro.getBaseDatos().cancelarAsistenciaActividad(lista);

						JOptionPane.showMessageDialog(null, mensaje);

					}
					
					
					
					

					
					
				}
			});
		}
		return btnDesapuntarse;
	}
	private JLabel getLblOperacionesDeAdministracion() {
		if (lblOperacionesDeAdministracion == null) {
			lblOperacionesDeAdministracion = new JLabel("Operaciones de Administraci\u00F3n");
			lblOperacionesDeAdministracion.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblOperacionesDeAdministracion;
	}
	private JButton getBtnEliminarActividad() {
		if (btnEliminarActividad == null) {
			btnEliminarActividad = new JButton("Eliminar actividad del calendario");
			btnEliminarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

//					String reservante = getTextFieldNombre().getText();
//
//					LocalDate fechaSeleccionada = getDateFromDatePicker();
//
//					String horario = getTextFieldHorario().getText();
//
//
//					String[] s = horario.split("-");
//					String horaInicio = s[0].replaceAll("\\s","");
//					String horaFin = s[1].replaceAll("\\s","");
//
//
//					String fechaHoraInicio = fechaSeleccionada + " " + horaInicio + ":00";
//					DateTimeFormatter formatterInicio = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//					LocalDateTime dateTimeInicio = LocalDateTime.parse(fechaHoraInicio, formatterInicio);
//
//					String fechaHoraFin = fechaSeleccionada + " " + horaFin +  ":00";
//					DateTimeFormatter formatterFin = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//					LocalDateTime dateTimeFin = LocalDateTime.parse(fechaHoraFin, formatterFin);
//
//					String mensaje = centro.getBaseDatos().eliminarActividadDiaConcreto(reservante, dateTimeInicio, dateTimeFin);
//
//					JOptionPane.showMessageDialog(null, mensaje);

					String reservante = getTextFieldNombre().getText();


					LocalDateTime dateTimeInicio = fechaSeleccionada;

					
					String año = String.valueOf((int) fechaSeleccionada.getYear());
					String mes = String.valueOf(fechaSeleccionada.getMonth().getValue());
					String dia =  String.valueOf(fechaSeleccionada.getDayOfMonth());
					String hora = "";
					if(fechaSeleccionada.getHour() < 10){
						hora = "0" + String.valueOf(fechaSeleccionada.getHour());
					}
					else{
						hora = String.valueOf(fechaSeleccionada.getHour());
					}
					String minutos = "59";
					String segundos = String.valueOf(fechaSeleccionada.getSecond()+"0");


					String fechaHoraFin = año + "-" + mes + "-" + dia + " " + hora + ":" + minutos + ":" + segundos;

					DateTimeFormatter formatterFin = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					LocalDateTime dateTimeFin = LocalDateTime.parse(fechaHoraFin, formatterFin);


					String error = Comprobaciones.comprobarFechaHoraCancelacion(dateTimeInicio, dateTimeFin, textFieldDni.getText(), centro.isAdmin());										

					if(!error.equals("")){
						JOptionPane.showMessageDialog(null, error);
					}
					else {			

						String s = String.valueOf(dateTimeInicio) + "-" + String.valueOf(dateTimeFin) + "-" + reservante + "-"
								+ String.valueOf(getComboInstalaciones().getSelectedIndex()+1);
						
						centro.getBaseDatos().eliminarListaActividadDiaConcreto(s);
						String mensaje = centro.getBaseDatos().eliminarActividadDiaConcreto(reservante, dateTimeInicio, dateTimeFin);

						JOptionPane.showMessageDialog(null, mensaje);

					}



				}
			});
		}
		return btnEliminarActividad;
	}
	private JPanel getPanelOperacionesAdmin() {
		if (panelOperacionesAdmin == null) {
			panelOperacionesAdmin = new JPanel();
			GridBagLayout gbl_panelOperacionesAdmin = new GridBagLayout();
			gbl_panelOperacionesAdmin.columnWidths = new int[]{99, 0};
			gbl_panelOperacionesAdmin.rowHeights = new int[]{45, 45, 45, 0};
			gbl_panelOperacionesAdmin.columnWeights = new double[]{0.0, 1.0};
			gbl_panelOperacionesAdmin.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelOperacionesAdmin.setLayout(gbl_panelOperacionesAdmin);
			GridBagConstraints gbc_btnAñadirActividad = new GridBagConstraints();
			gbc_btnAñadirActividad.insets = new Insets(0, 0, 5, 0);
			gbc_btnAñadirActividad.gridwidth = 2;
			gbc_btnAñadirActividad.gridx = 0;
			gbc_btnAñadirActividad.gridy = 0;
			panelOperacionesAdmin.add(getBtnAñadirActividad(), gbc_btnAñadirActividad);
			GridBagConstraints gbc_btnEliminarActividad = new GridBagConstraints();
			gbc_btnEliminarActividad.insets = new Insets(0, 0, 5, 0);
			gbc_btnEliminarActividad.gridwidth = 2;
			gbc_btnEliminarActividad.gridx = 0;
			gbc_btnEliminarActividad.gridy = 1;
			panelOperacionesAdmin.add(getBtnEliminarActividad(), gbc_btnEliminarActividad);
		}
		return panelOperacionesAdmin;
	}
	private JPanel getPanelCardOperacionesAdmin() {
		if (panelCardOperacionesAdmin == null) {
			panelCardOperacionesAdmin = new JPanel();
			panelCardOperacionesAdmin.setLayout(new CardLayout(0, 0));
			panelCardOperacionesAdmin.add(getPanelOperacionesAdmin(), "panelOperacionesAdmin");
			panelCardOperacionesAdmin.add(getPanelAñadirActividad(), "panelAñadirActividad");
		}
		return panelCardOperacionesAdmin;
	}
	private JPanel getPanelAñadirActividad() {
		if (panelAñadirActividad == null) {
			panelAñadirActividad = new JPanel();
			GridBagLayout gbl_panelAñadirActividad = new GridBagLayout();
			gbl_panelAñadirActividad.columnWidths = new int[]{100, 80, 70, 100};
			gbl_panelAñadirActividad.rowHeights = new int[]{40, 30, 40, 40, 40, 0};
			gbl_panelAñadirActividad.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0};
			gbl_panelAñadirActividad.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelAñadirActividad.setLayout(gbl_panelAñadirActividad);
			GridBagConstraints gbc_lblId = new GridBagConstraints();
			gbc_lblId.anchor = GridBagConstraints.WEST;
			gbc_lblId.insets = new Insets(0, 0, 5, 5);
			gbc_lblId.gridx = 0;
			gbc_lblId.gridy = 0;
			panelAñadirActividad.add(getLblId(), gbc_lblId);
			GridBagConstraints gbc_comboBoxActividades = new GridBagConstraints();
			gbc_comboBoxActividades.gridwidth = 3;
			gbc_comboBoxActividades.insets = new Insets(0, 0, 5, 0);
			gbc_comboBoxActividades.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBoxActividades.gridx = 1;
			gbc_comboBoxActividades.gridy = 0;
			panelAñadirActividad.add(getComboBoxActividades(), gbc_comboBoxActividades);
			GridBagConstraints gbc_lblFechaInicial = new GridBagConstraints();
			gbc_lblFechaInicial.anchor = GridBagConstraints.WEST;
			gbc_lblFechaInicial.insets = new Insets(0, 0, 5, 5);
			gbc_lblFechaInicial.gridx = 0;
			gbc_lblFechaInicial.gridy = 1;
			panelAñadirActividad.add(getLblFechaInicial(), gbc_lblFechaInicial);
			GridBagConstraints gbc_listFecha = new GridBagConstraints();
			gbc_listFecha.gridwidth = 3;
			gbc_listFecha.insets = new Insets(0, 0, 5, 0);
			gbc_listFecha.fill = GridBagConstraints.BOTH;
			gbc_listFecha.gridx = 1;
			gbc_listFecha.gridy = 1;
			panelAñadirActividad.add(getListFecha(), gbc_listFecha);
			GridBagConstraints gbc_lblDesdeAct = new GridBagConstraints();
			gbc_lblDesdeAct.anchor = GridBagConstraints.WEST;
			gbc_lblDesdeAct.insets = new Insets(0, 0, 5, 5);
			gbc_lblDesdeAct.gridx = 0;
			gbc_lblDesdeAct.gridy = 2;
			panelAñadirActividad.add(getLblDesdeAct(), gbc_lblDesdeAct);
			GridBagConstraints gbc_comboBoxDesdeAct = new GridBagConstraints();
			gbc_comboBoxDesdeAct.insets = new Insets(0, 0, 5, 5);
			gbc_comboBoxDesdeAct.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBoxDesdeAct.gridx = 1;
			gbc_comboBoxDesdeAct.gridy = 2;
			panelAñadirActividad.add(getComboBoxDesdeAct(), gbc_comboBoxDesdeAct);
			GridBagConstraints gbc_lblHastaAct = new GridBagConstraints();
			gbc_lblHastaAct.insets = new Insets(0, 0, 5, 5);
			gbc_lblHastaAct.gridx = 2;
			gbc_lblHastaAct.gridy = 2;
			panelAñadirActividad.add(getLblHastaAct(), gbc_lblHastaAct);
			GridBagConstraints gbc_comboBoxHastaAct = new GridBagConstraints();
			gbc_comboBoxHastaAct.insets = new Insets(0, 0, 5, 0);
			gbc_comboBoxHastaAct.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBoxHastaAct.gridx = 3;
			gbc_comboBoxHastaAct.gridy = 2;
			panelAñadirActividad.add(getComboBoxHastaAct(), gbc_comboBoxHastaAct);
			GridBagConstraints gbc_lblPlazasOfrecidas = new GridBagConstraints();
			gbc_lblPlazasOfrecidas.anchor = GridBagConstraints.WEST;
			gbc_lblPlazasOfrecidas.insets = new Insets(0, 0, 5, 5);
			gbc_lblPlazasOfrecidas.gridx = 0;
			gbc_lblPlazasOfrecidas.gridy = 3;
			panelAñadirActividad.add(getLblPlazasOfrecidas(), gbc_lblPlazasOfrecidas);
			GridBagConstraints gbc_textFieldPlazas = new GridBagConstraints();
			gbc_textFieldPlazas.insets = new Insets(0, 0, 5, 5);
			gbc_textFieldPlazas.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldPlazas.gridx = 1;
			gbc_textFieldPlazas.gridy = 3;
			panelAñadirActividad.add(getTextFieldPlazas(), gbc_textFieldPlazas);
			GridBagConstraints gbc_lblMonitor = new GridBagConstraints();
			gbc_lblMonitor.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonitor.gridx = 2;
			gbc_lblMonitor.gridy = 3;
			panelAñadirActividad.add(getLblMonitor(), gbc_lblMonitor);
			GridBagConstraints gbc_txtMonitor = new GridBagConstraints();
			gbc_txtMonitor.insets = new Insets(0, 0, 5, 0);
			gbc_txtMonitor.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtMonitor.gridx = 3;
			gbc_txtMonitor.gridy = 3;
			panelAñadirActividad.add(getTxtMonitor(), gbc_txtMonitor);
			GridBagConstraints gbc_lblNDeSemanas = new GridBagConstraints();
			gbc_lblNDeSemanas.anchor = GridBagConstraints.WEST;
			gbc_lblNDeSemanas.insets = new Insets(0, 0, 0, 5);
			gbc_lblNDeSemanas.gridx = 0;
			gbc_lblNDeSemanas.gridy = 4;
			panelAñadirActividad.add(getLblNDeSemanas(), gbc_lblNDeSemanas);
			GridBagConstraints gbc_textFieldSemanas = new GridBagConstraints();
			gbc_textFieldSemanas.gridwidth = 2;
			gbc_textFieldSemanas.insets = new Insets(0, 0, 0, 5);
			gbc_textFieldSemanas.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldSemanas.gridx = 1;
			gbc_textFieldSemanas.gridy = 4;
			panelAñadirActividad.add(getTextFieldSemanas(), gbc_textFieldSemanas);
			GridBagConstraints gbc_btnAceptare = new GridBagConstraints();
			gbc_btnAceptare.gridx = 3;
			gbc_btnAceptare.gridy = 4;
			panelAñadirActividad.add(getBtnAceptare(), gbc_btnAceptare);
		}
		return panelAñadirActividad;
	}
	private JLabel getLblFechaInicial() {
		if (lblFechaInicial == null) {
			lblFechaInicial = new JLabel("Fecha Inicial:");
		}
		return lblFechaInicial;
	}
	private JLabel getLblNDeSemanas() {
		if (lblNDeSemanas == null) {
			lblNDeSemanas = new JLabel("N\u00BA de semanas:");
		}
		return lblNDeSemanas;
	}
	private JTextField getTextFieldSemanas() {
		if (textFieldSemanas == null) {
			textFieldSemanas = new JTextField();
			textFieldSemanas.setText("0");
			textFieldSemanas.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldSemanas.setColumns(10);
		}
		return textFieldSemanas;
	}
	private JLabel getLblId() {
		if (lblId == null) {
			lblId = new JLabel("Actividad:");
		}
		return lblId;
	}

	private JButton getBtnAceptare() {

		if (btnAceptare == null) {
			btnAceptare = new JButton("Aceptar");
			btnAceptare.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				}
			});
			btnAceptare.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {			
					
					String error = "";
					if (listFecha.getSelectedValue() == null) 
						error += "- Debe clicar en el botón con la fecha inicial que desea\n";
					
					LocalTime desde = (LocalTime) comboBoxDesdeAct.getSelectedItem();
					LocalTime hasta = (LocalTime) comboBoxHastaAct.getSelectedItem();
					
					if (desde.isAfter(hasta) || desde.equals(hasta))
						error += "- La hora de inicio debe ser anterior a la hora de fin\n";
					
					if (!error.equals(""))
						JOptionPane.showMessageDialog(null, error);

					else {

						LocalDateTime fechaInicio = LocalDateTime.of(listFecha.getSelectedValue(), desde);
						LocalDateTime fechaFin = LocalDateTime.of(listFecha.getSelectedValue(), hasta);	
						String instalacion = ((Instalacion) comboBoxInstalaciones.getSelectedItem()).getCodInstalacion();
						String monitor = txtMonitor.getText();	

						for (int i = 0; i < Integer.parseInt(textFieldSemanas.getText()); i++) {							

							if (comprobarMonitor(fechaInicio,fechaFin,monitor)){
								JOptionPane.showMessageDialog(null, b);
								return;
							}

							Reserva colision = Comprobaciones.getReservaEnColision(fechaInicio, fechaFin, instalacion);	
							boolean añadir = true;													
							
							if (colision != null) {
								
								boolean[] decisiones = añadirNuevaReservaEnCasoDeColision(colision);
								
								if (decisiones[1] == true)
									return;
								añadir = decisiones[0];								
							}							

							if (añadir) {		
								
								//Datos que necesito guardar en la reserva									
								String actividad = ((Actividad) comboBoxActividades.getSelectedItem()).getIdActividad();								
								int plazas = 0;
								
								if (textFieldPlazas.getText().equals(""))
									plazas = 99999;
								else 
									plazas = Integer.parseInt(textFieldPlazas.getText());
								
								Reserva rs = new Reserva(instalacion, actividad, fechaInicio, fechaFin, null, null, 0, true, false, monitor, plazas);	
								centro.getBaseDatos().guardarReserva(rs);
								
								JOptionPane.showMessageDialog(null, rs.mostrarDetallesParaActividades());							
							}		
							
							fechaInicio = fechaInicio.plusDays(7);
							fechaFin = fechaFin.plusDays(7);
						}				
					}
					creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());
				}				
			});			
		}
		return btnAceptare;
	}
 
	public boolean comprobarMonitor (LocalDateTime i, LocalDateTime f, String dni){
		boolean aux = false;
		for (Reserva r: centro.getReservas()) {
				if (r.getMonitor() != null) {
					if (r.getMonitor().equals(dni) && i.equals(r.getInicioReserva())){
						this.b = "Éste monitor tiene asignada otra actividad a la misma hora";
						return true;
					}
					if (r.getMonitor().equals(dni) && r.getInicioReserva().isBefore(i) && r.getFinReserva().isAfter(i)){
						this.b="Éste monitor tiene asignada una actividad que ha empezado antes que ésta";
						return true;
					}
					if (r.getMonitor().equals(dni) && i.isBefore(r.getInicioReserva()) && f.isAfter(r.getInicioReserva()) ){
						this.b ="Éste monitor tiene asignada una actividad que empezará mientras la que acabas de reservar se lleva a cabo";
						return true;
					}
			}
		} 
		return aux;
	}


	public String rellenaAvisos() throws SQLException{

		List<ReservaCancelada> reservasCanceladas = centro.getReservasCanceladas();
		String avisos = "";
		for (ReservaCancelada r : reservasCanceladas){
			if(r.isAvisado() == false){
				Socio socio = centro.getBaseDatos().getSocioConDni(r.getDni());
				String nombreInstalacion = centro.getBaseDatos().getInstalacionConCodigo(r.codInstalacion).getNombre();

				avisos += "DNI: " + r.getDni() + " --- Socio: " + socio.getNombre() + " " + socio.getApellidos() + " --- " + 
						nombreInstalacion + " --- " + r.getCausa() + "\n";

			}
		}
		return avisos;
	}

	private JButton getButtonSemanaAnterior() {
		if (buttonSemanaAnterior == null) {
			buttonSemanaAnterior = new JButton("<<");
			buttonSemanaAnterior.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					LocalDate fechaActualMostrada = getDateFromDatePicker();
					fechaActualMostrada = fechaActualMostrada.minusDays(7);

					String error = Comprobaciones.comprobarFechaPasada(fechaActualMostrada, centro.isAdmin());		
					if (!error.equals(""))
						JOptionPane.showMessageDialog(null, error);
					else {
						setDateIntoDatePicker(fechaActualMostrada);
						if (!rdbtnMonitor.isSelected())
							creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());						
						else
							creaBotonesOcupacionMonitor(panelVerHoras, getDateFromDatePicker());
					}				
				}
			});
		}
		return buttonSemanaAnterior;
	}
	private JButton getButtonSemanaSiguiente() {
		if (buttonSemanaSiguiente == null) {
			buttonSemanaSiguiente = new JButton(">>");
			buttonSemanaSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					LocalDate fechaActualMostrada = getDateFromDatePicker();
					fechaActualMostrada = fechaActualMostrada.plusDays(7);					

					setDateIntoDatePicker(fechaActualMostrada);
					if (!rdbtnMonitor.isSelected())
						creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());						
					else
						creaBotonesOcupacionMonitor(panelVerHoras, getDateFromDatePicker());
				}
			});


		}
		return buttonSemanaSiguiente;
	}
	private JPanel getPanelListaActividades() {
		if (panelListaActividades == null) {
			panelListaActividades = new JPanel();
			GridBagLayout gbl_panelListaActividades = new GridBagLayout();
			gbl_panelListaActividades.columnWidths = new int[]{0, 0};
			gbl_panelListaActividades.rowHeights = new int[]{0, 45, 0, 0};
			gbl_panelListaActividades.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panelListaActividades.rowWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
			panelListaActividades.setLayout(gbl_panelListaActividades);
			GridBagConstraints gbc_scrollPaneActividades = new GridBagConstraints();
			gbc_scrollPaneActividades.insets = new Insets(0, 0, 5, 0);
			gbc_scrollPaneActividades.fill = GridBagConstraints.BOTH;
			gbc_scrollPaneActividades.gridx = 0;
			gbc_scrollPaneActividades.gridy = 0;
			panelListaActividades.add(getScrollPaneActividades(), gbc_scrollPaneActividades);
			GridBagConstraints gbc_btnContinuarLista = new GridBagConstraints();
			gbc_btnContinuarLista.insets = new Insets(0, 0, 5, 0);
			gbc_btnContinuarLista.anchor = GridBagConstraints.EAST;
			gbc_btnContinuarLista.gridx = 0;
			gbc_btnContinuarLista.gridy = 1;
			panelListaActividades.add(getBtnContinuarLista(), gbc_btnContinuarLista);
			GridBagConstraints gbc_scrollPaneLista = new GridBagConstraints();
			gbc_scrollPaneLista.fill = GridBagConstraints.BOTH;
			gbc_scrollPaneLista.gridx = 0;
			gbc_scrollPaneLista.gridy = 2;
			panelListaActividades.add(getScrollPaneLista(), gbc_scrollPaneLista);
		}
		return panelListaActividades;
	}
	private JScrollPane getScrollPaneActividades() {
		if (scrollPaneActividades == null) {
			scrollPaneActividades = new JScrollPane();
			scrollPaneActividades.setColumnHeaderView(getLblSeleccioneLaActividad());
			scrollPaneActividades.setViewportView(getListaActividades());
		}
		return scrollPaneActividades;
	}
	private JLabel getLblSeleccioneLaActividad() {
		if (lblSeleccioneLaActividad == null) {
			lblSeleccioneLaActividad = new JLabel("Tus Actividades (se muestran las de hoy):");
			lblSeleccioneLaActividad.setBorder(new EmptyBorder(5, 0, 5, 0));
			lblSeleccioneLaActividad.setHorizontalAlignment(SwingConstants.CENTER);
			lblSeleccioneLaActividad.setFont(new Font("Calibri", Font.BOLD, 16));
		}
		return lblSeleccioneLaActividad;
	}
	private JButton getBtnContinuarLista() {
		if (btnContinuarLista == null) {
			btnContinuarLista = new JButton("Continuar");
			btnContinuarLista.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					if (listaActividades.isSelectionEmpty())
						JOptionPane.showMessageDialog(null, "- Debe seleccionar una actividad para poder continuar");
						
					else {
						generarModeloListLista();		
						scrollPaneLista.setVisible(true);
						getContentPane().validate();
						getContentPane().repaint();
					}	
				}
			});
		}
		return btnContinuarLista;
	}
	private JList<Reserva> getListaActividades() {
		if (listaActividades == null) {
			
			listaActividades = new JList<Reserva>();		
			listaActividades.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			listaActividades.setCellRenderer(new ActividadesRenderer());	
			
		}
		return listaActividades;
	}
	
	public void generaModeloListaActividades() {
		
		DefaultListModel<Reserva> model = new DefaultListModel<Reserva>();
		for (Reserva rs : centro.getBaseDatos().getReservasActividadDeHoyPorMonitor(textFieldDni.getText())) {
			if (rs.getNumPlazas() != 99999)
				model.addElement(rs);		
		}
		listaActividades.setModel(model);
	}
	
	private JButton getBtnPasarListaA() {
		if (btnPasarListaA == null) {
			btnPasarListaA = new JButton("Pasar lista a una actividad");
			btnPasarListaA.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					generaModeloListaActividades();
					CardLayout card = (CardLayout) panelAccionesMonitor.getLayout();
					card.show(panelAccionesMonitor, "panelListaActividades");	
				}
			});
		}
		return btnPasarListaA;
	}

	public void generarModeloListLista() {

		DefaultListModel<Lista> model = new DefaultListModel<Lista>();
		
		for (Lista li : centro.getBaseDatos().getListasPorReserva(listaActividades.getSelectedValue()))
			model.addElement(li);	
		
		listLista.setModel(model);		
	}
	

	private JPanel getPanelDatePicker() {

		if (panelDatePicker == null) {
			panelDatePicker = new JPanel();
			GridBagLayout gbl_panelDatePicker = new GridBagLayout();
			gbl_panelDatePicker.columnWidths = new int[]{20, 100, 15, 80, 15, 0};
			gbl_panelDatePicker.rowHeights = new int[]{0, 0};
			gbl_panelDatePicker.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panelDatePicker.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			panelDatePicker.setLayout(gbl_panelDatePicker);			
			datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());			
			GridBagConstraints gbc_datePicker = new GridBagConstraints();
			gbc_datePicker.fill = GridBagConstraints.BOTH;
			gbc_datePicker.insets = new Insets(0, 0, 0, 5);
			gbc_datePicker.gridx = 1;
			gbc_datePicker.gridy = 0;
			panelDatePicker.add(datePicker, gbc_datePicker);
			datePicker.setFont(new Font("Tahoma", Font.PLAIN, 12));
			GridBagConstraints gbc_btnActualizarHoras = new GridBagConstraints();
			gbc_btnActualizarHoras.insets = new Insets(0, 0, 0, 5);
			gbc_btnActualizarHoras.gridx = 3;
			gbc_btnActualizarHoras.gridy = 0;
			panelDatePicker.add(getBtnActualizarHoras(), gbc_btnActualizarHoras);

		}
		return panelDatePicker;
	}
	private JLabel getLblDisponibilidad() {
		if (lblDisponibilidad == null) {
			lblDisponibilidad = new JLabel();
			lblDisponibilidad.setHorizontalAlignment(SwingConstants.CENTER);
			lblDisponibilidad.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblDisponibilidad;
	}
	private JLabel getLlblVerCalendario() {
		if (llblVerCalendario == null) {
			llblVerCalendario = new JLabel("Ver calendario semanal de:");
			llblVerCalendario.setHorizontalAlignment(SwingConstants.CENTER);
			llblVerCalendario.setFont(new Font("Calibri", Font.BOLD, 16));
			llblVerCalendario.setBorder(new EmptyBorder(4, 4, 4, 4));
		}
		return llblVerCalendario;
	}
	private JPanel getPanelMonitor() {
		if (panelMonitor == null) {
			panelMonitor = new JPanel();
			GridBagLayout gbl_panelMonitor = new GridBagLayout();
			gbl_panelMonitor.columnWidths = new int[]{0, 0};
			gbl_panelMonitor.rowHeights = new int[]{0, 0};
			gbl_panelMonitor.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panelMonitor.rowWeights = new double[]{1.0, Double.MIN_VALUE};
			panelMonitor.setLayout(gbl_panelMonitor);
			GridBagConstraints gbc_panelAccionesMonitor = new GridBagConstraints();
			gbc_panelAccionesMonitor.fill = GridBagConstraints.BOTH;
			gbc_panelAccionesMonitor.gridx = 0;
			gbc_panelAccionesMonitor.gridy = 0;
			panelMonitor.add(getPanelAccionesMonitor(), gbc_panelAccionesMonitor);
		}
		return panelMonitor;
	}
	private JPanel getPanelAccionesMonitor() {
		if (panelAccionesMonitor == null) {
			panelAccionesMonitor = new JPanel();
			panelAccionesMonitor.setLayout(new CardLayout(0, 0));
			panelAccionesMonitor.add(getPanelBotonesMonitor(), "panelBotonesMonitor");
			panelAccionesMonitor.add(getPanelListaActividades(), "panelListaActividades");
			panelAccionesMonitor.add(getPanelAñadirGenteDeCola(), "panelAñadirGenteDeCola");
		}
		return panelAccionesMonitor;
	}
	private JPanel getPanelBotonesMonitor() {
		if (panelBotonesMonitor == null) {
			panelBotonesMonitor = new JPanel();
			GridBagLayout gbl_panelBotonesMonitor = new GridBagLayout();
			gbl_panelBotonesMonitor.columnWidths = new int[]{157, 0};
			gbl_panelBotonesMonitor.rowHeights = new int[]{35, 35, 35, 0};
			gbl_panelBotonesMonitor.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panelBotonesMonitor.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelBotonesMonitor.setLayout(gbl_panelBotonesMonitor);
			GridBagConstraints gbc_lblAccionesDisponibles = new GridBagConstraints();
			gbc_lblAccionesDisponibles.anchor = GridBagConstraints.NORTH;
			gbc_lblAccionesDisponibles.insets = new Insets(0, 0, 5, 0);
			gbc_lblAccionesDisponibles.gridx = 0;
			gbc_lblAccionesDisponibles.gridy = 0;
			panelBotonesMonitor.add(getLblAccionesDisponibles(), gbc_lblAccionesDisponibles);
			GridBagConstraints gbc_btnVerLista = new GridBagConstraints();
			gbc_btnVerLista.insets = new Insets(0, 0, 5, 0);
			gbc_btnVerLista.gridx = 0;
			gbc_btnVerLista.gridy = 1;
			panelBotonesMonitor.add(getBtnVerLista(), gbc_btnVerLista);
			GridBagConstraints gbc_btnPasarListaA = new GridBagConstraints();
			gbc_btnPasarListaA.gridx = 0;
			gbc_btnPasarListaA.gridy = 2;
			panelBotonesMonitor.add(getBtnPasarListaA(), gbc_btnPasarListaA);
		}
		return panelBotonesMonitor;
	}
	private JButton getBtnVerLista() {
		if (btnVerLista == null) {
			btnVerLista = new JButton("Ver lista");
			btnVerLista.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					CardLayout card = (CardLayout) panelAccionesMonitor.getLayout();
					card.show(panelAccionesMonitor, "panelAñadirGenteDeCola");	
					
					listActividades.setVisible(true);
					listActividades.setEnabled(true);
					sPActividades.setEnabled(true);
					sPActividades.setVisible(true);
					lblActividades.setVisible(true);
					btnOk2.setVisible(true);
					sPPersonas.setVisible(true);
					preAct();
				}
			});
		}
		return btnVerLista;
	}
	private JLabel getTxtPlazas() {
		if (txtPlazas == null) {
			txtPlazas = new JLabel("");
		}
		return txtPlazas;
	}
	private JLabel getLblMonitor() {
		if (lblMonitor == null) {
			lblMonitor = new JLabel("Monitor:");
		}
		return lblMonitor;
	}
	private JTextField getTxtMonitor() {
		if (txtMonitor == null) {
			txtMonitor = new JTextField();
			txtMonitor.setHorizontalAlignment(SwingConstants.CENTER);
			txtMonitor.setColumns(10);
		}
		return txtMonitor;
	}
	private JButton getBtnAvisos() {
		if (btnAvisos == null) {
			btnAvisos = new JButton("Avisos a socios");
			btnAvisos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						String mensaje = "";
						if(rellenaAvisos().isEmpty()){
							mensaje = "No hay avisos que mostrar";
						}
						else{
							mensaje = rellenaAvisos();
						}


						JOptionPane.showMessageDialog(null, mensaje);

					} catch (HeadlessException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					}

				}
			});
			btnAvisos.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return btnAvisos;
	}
	private JLabel getLblDesdeAct() {
		if (lblDesdeAct == null) {
			lblDesdeAct = new JLabel("Hora de Inicio:");
		}
		return lblDesdeAct;
	}
	private JLabel getLblHastaAct() {
		if (lblHastaAct == null) {
			lblHastaAct = new JLabel("Hora de Fin:");
		}
		return lblHastaAct;
	}
	private JComboBox<LocalTime> getComboBoxDesdeAct() {
		if (comboBoxDesdeAct == null) {
			
			DefaultComboBoxModel<LocalTime> horas = new DefaultComboBoxModel<LocalTime>();
			for (LocalTime i : getHorasDia())
				horas.addElement(i);
			
			comboBoxDesdeAct = new JComboBox<LocalTime>(horas);
		}
		return comboBoxDesdeAct;
	}
	private JComboBox<LocalTime> getComboBoxHastaAct() {
		if (comboBoxHastaAct == null) {
			
			DefaultComboBoxModel<LocalTime> horas = new DefaultComboBoxModel<LocalTime>();
			for (LocalTime i : getHorasDia())
				horas.addElement(i.plusMinutes(59));
			
			comboBoxHastaAct = new JComboBox<LocalTime>(horas);
		}
		return comboBoxHastaAct;
	}
	private JComboBox<Actividad> getComboBoxActividades() {
		if (comboBoxActividades == null) {
			
			DefaultComboBoxModel<Actividad> model = new DefaultComboBoxModel<Actividad>();
			for (Actividad a: centro.getActividades())
				model.addElement(a);
			
			comboBoxActividades = new JComboBox<Actividad>(model);	
		}
		return comboBoxActividades;
	}
	private JList<LocalDate> getListFecha() {
		if (listFecha == null) {
			listFecha = new JList<LocalDate>();
			listFecha.setSelectionForeground(Color.BLACK);
			listFecha.setSelectionBackground(Color.WHITE);
			listFecha.setBorder(new LineBorder(UIManager.getColor("Panel.background"), 3));
		}
		return listFecha;
	}
	private JLabel getLblPlazasOfrecidas() {
		if (lblPlazasOfrecidas == null) {
			lblPlazasOfrecidas = new JLabel("Plazas Ofrecidas:");
		}
		return lblPlazasOfrecidas;
	}
	private JTextField getTextFieldPlazas() {
		if (textFieldPlazas == null) {
			textFieldPlazas = new JTextField();
			textFieldPlazas.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldPlazas.setText("0");
			textFieldPlazas.setColumns(10);
		}
		return textFieldPlazas;
	}
	private JRadioButton getRdbtnMonitor() {
		if (rdbtnMonitor == null) {
			rdbtnMonitor = new JRadioButton("Monitor");
			
			rdbtnMonitor.setVisible(false);
			buttonGroup2.add(rdbtnMonitor);
		}
		return rdbtnMonitor;
	}
	private JLabel getLblAccionesDisponibles() {
		if (lblAccionesDisponibles == null) {
			lblAccionesDisponibles = new JLabel("Acciones Disponibles:");
			lblAccionesDisponibles.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblAccionesDisponibles;
	}
	private JPanel getPanelVerHoras() {
		if (panelVerHoras == null) {
			panelVerHoras = new JPanel();
			GridBagLayout gbl_panelVerHoras = new GridBagLayout();
			gbl_panelVerHoras.columnWidths = new int[]{0};
			gbl_panelVerHoras.rowHeights = new int[]{0};
			gbl_panelVerHoras.columnWeights = new double[]{Double.MIN_VALUE};
			gbl_panelVerHoras.rowWeights = new double[]{Double.MIN_VALUE};
			panelVerHoras.setLayout(gbl_panelVerHoras);
		}
		return panelVerHoras;
	}
	private JButton getBtnActualizarHoras() {
		if (btnActualizarHoras == null) {
			btnActualizarHoras = new JButton("Actualizar");
			btnActualizarHoras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					if (!rdbtnMonitor.isSelected())
						creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());						
					else
						creaBotonesOcupacionMonitor(panelVerHoras, getDateFromDatePicker());
				}
			});
		}
		return btnActualizarHoras;
	}
	private JScrollPane getScrollPaneLista() {
		if (scrollPaneLista == null) {
			scrollPaneLista = new JScrollPane();
			scrollPaneLista.setColumnHeaderView(getLabelPresionaParaModificar());
			scrollPaneLista.setViewportView(getListLista());
			
			scrollPaneLista.setVisible(false);
		}
		return scrollPaneLista;
	}
	private JLabel getLabelPresionaParaModificar() {
		if (labelPresionaParaModificar == null) {
			labelPresionaParaModificar = new JLabel("Presiona para modificar la asistencia");
			labelPresionaParaModificar.setBorder(new EmptyBorder(5, 0, 5, 0));
			labelPresionaParaModificar.setHorizontalAlignment(SwingConstants.CENTER);
			labelPresionaParaModificar.setFont(new Font("Calibri", Font.BOLD, 16));
		}
		return labelPresionaParaModificar;
	}
	private JList<Lista> getListLista() {
		if (listLista == null) {
			listLista = new JList<Lista>();
			
			listLista.addListSelectionListener(new ListSelectionListener() {

				public void valueChanged(ListSelectionEvent listSelectionEvent) {

					Lista li = listLista.getSelectedValue();
					if (li != null) {
						if (!centro.getBaseDatos().actualizarAsistenciaSocio(listLista.getSelectedValue()))
							JOptionPane.showMessageDialog(null, "Error al actualizar la asitencia del socio");

						generarModeloListLista();
						listLista.clearSelection();
					}
				}
			});
		}
		return listLista;
	}
	private JButton getBtnReservarEventos() {
		if (btnReservarEventos == null) {
			btnReservarEventos = new JButton("Reservar para eventos extraordinarios");
			btnReservarEventos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					LocalDateTime fechaInicioSeleccionada = getFechaCompletaSeleccionada(getComboBoxDesde());
					LocalDateTime fechaFinSeleccionada = getFechaCompletaSeleccionada(getComboBoxHasta());

					String error = Comprobaciones.comprobarFechasValidas(fechaInicioSeleccionada, fechaFinSeleccionada, textFieldDni.getText(), centro.isAdmin());	
					if (!error.equals("")) 
						JOptionPane.showMessageDialog(null, error);
					
					else {					
						CardLayout cardLayout = (CardLayout) panelOperacion.getLayout();
						cardLayout.show(panelOperacion, "panelReservarEventos");
					}
				}
			});
		}
		return btnReservarEventos;
	}
	private JPanel getPanelReservarEventos() {
		if (panelReservarEventos == null) {
			panelReservarEventos = new JPanel();
			GridBagLayout gbl_panelReservarEventos = new GridBagLayout();
			gbl_panelReservarEventos.columnWidths = new int[]{100, 10, 50, 10, 80, 0};
			gbl_panelReservarEventos.rowHeights = new int[]{35, 45, 45, 0};
			gbl_panelReservarEventos.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panelReservarEventos.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelReservarEventos.setLayout(gbl_panelReservarEventos);
			GridBagConstraints gbc_lblDatosReserva = new GridBagConstraints();
			gbc_lblDatosReserva.gridwidth = 5;
			gbc_lblDatosReserva.insets = new Insets(0, 0, 5, 0);
			gbc_lblDatosReserva.gridx = 0;
			gbc_lblDatosReserva.gridy = 0;
			panelReservarEventos.add(getLblDatosReserva(), gbc_lblDatosReserva);
			GridBagConstraints gbc_lblNombreEvento = new GridBagConstraints();
			gbc_lblNombreEvento.anchor = GridBagConstraints.WEST;
			gbc_lblNombreEvento.insets = new Insets(0, 0, 5, 5);
			gbc_lblNombreEvento.gridx = 0;
			gbc_lblNombreEvento.gridy = 1;
			panelReservarEventos.add(getLblNombreEvento(), gbc_lblNombreEvento);
			GridBagConstraints gbc_textFieldEvento = new GridBagConstraints();
			gbc_textFieldEvento.gridwidth = 4;
			gbc_textFieldEvento.insets = new Insets(0, 0, 5, 0);
			gbc_textFieldEvento.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldEvento.gridx = 1;
			gbc_textFieldEvento.gridy = 1;
			panelReservarEventos.add(getTextFieldEvento(), gbc_textFieldEvento);
			GridBagConstraints gbc_lblSemanas = new GridBagConstraints();
			gbc_lblSemanas.anchor = GridBagConstraints.WEST;
			gbc_lblSemanas.insets = new Insets(0, 0, 0, 5);
			gbc_lblSemanas.gridx = 0;
			gbc_lblSemanas.gridy = 2;
			panelReservarEventos.add(getLblSemanas(), gbc_lblSemanas);
			GridBagConstraints gbc_textFieldNumSemanas = new GridBagConstraints();
			gbc_textFieldNumSemanas.insets = new Insets(0, 0, 0, 5);
			gbc_textFieldNumSemanas.fill = GridBagConstraints.HORIZONTAL;
			gbc_textFieldNumSemanas.gridx = 2;
			gbc_textFieldNumSemanas.gridy = 2;
			panelReservarEventos.add(getTextFieldNumSemanas(), gbc_textFieldNumSemanas);
			GridBagConstraints gbc_btnReservarEvento = new GridBagConstraints();
			gbc_btnReservarEvento.gridx = 4;
			gbc_btnReservarEvento.gridy = 2;
			panelReservarEventos.add(getBtnReservarEvento(), gbc_btnReservarEvento);
		}
		return panelReservarEventos;
	}
	private JLabel getLblDatosReserva() {
		if (lblDatosReserva == null) {
			lblDatosReserva = new JLabel("Informaci\u00F3n adicional:");
			lblDatosReserva.setFont(new Font("Calibri", Font.BOLD, 18));
		}
		return lblDatosReserva;
	}
	private JLabel getLblNombreEvento() {
		if (lblNombreEvento == null) {
			lblNombreEvento = new JLabel("Nombre del evento:");
		}
		return lblNombreEvento;
	}
	private JLabel getLblSemanas() {
		if (lblSemanas == null) {
			lblSemanas = new JLabel("N\u00FAmero de semanas:");
		}
		return lblSemanas;
	}
	private JTextField getTextFieldNumSemanas() {
		if (textFieldNumSemanas == null) {
			textFieldNumSemanas = new JTextField();
			textFieldNumSemanas.setText("0");
			textFieldNumSemanas.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldNumSemanas.setColumns(10);
		}
		return textFieldNumSemanas;
	}
	private JTextField getTextFieldEvento() {
		if (textFieldEvento == null) {
			textFieldEvento = new JTextField();
			textFieldEvento.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldEvento.setColumns(10);
		}
		return textFieldEvento;
	}
	private JButton getBtnReservarEvento() {
		if (btnReservarEvento == null) {
			btnReservarEvento = new JButton("Reservar");
			btnReservarEvento.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {				
												
					
					LocalTime desde = (LocalTime) comboBoxDesde.getSelectedItem();
					LocalTime hasta = (LocalTime) comboBoxHasta.getSelectedItem();
					
					if (desde.isAfter(hasta) || desde.equals(hasta))
						JOptionPane.showMessageDialog(null, "- La hora de inicio debe ser anterior a la hora de fin\n");

					else {

						LocalDateTime fechaInicio = LocalDateTime.of((LocalDate) comboBoxFecha.getSelectedItem(), desde);
						LocalDateTime fechaFin = LocalDateTime.of((LocalDate) comboBoxFecha.getSelectedItem(), hasta);	
						String instalacion = ((Instalacion) comboBoxInstalaciones.getSelectedItem()).getCodInstalacion();
						
						for (int i = 0; i < Integer.parseInt(textFieldNumSemanas.getText()); i++) {
							
							Reserva colision = Comprobaciones.getReservaEnColision(fechaInicio, fechaFin, instalacion);	
							boolean añadir = true;													
							
							if (colision != null) {
								
								boolean[] decisiones = añadirNuevaReservaEnCasoDeColision(colision);
								
								if (decisiones[1] == true)
									return;
								añadir = decisiones[0];								
							}							

							if (añadir) {										
								
								//Datos que necesito guardar en la reserva							
								Reserva rs = new Reserva(instalacion, textFieldEvento.getText(), fechaInicio, fechaFin, null, null, 0, true, false, null, 0);	
								centro.getBaseDatos().guardarReserva(rs);
								
								JOptionPane.showMessageDialog(null, rs.mostrarDetallesParaInstalaciones(centro.isAdmin(), null));							
							}		
							
							fechaInicio = fechaInicio.plusDays(7);
							fechaFin = fechaFin.plusDays(7);
						}				
					}
					creaBotonesOcupacionSemana(panelVerHoras, (Instalacion) comboBoxInstalaciones.getSelectedItem(), getDateFromDatePicker(), rdbtnReservaDeInstalaciones.isSelected());
				}				
			});			
		}
		return btnReservarEvento;
	}
}


