package igu;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import logica.Reserva;

public class ActividadesRenderer extends JLabel implements ListCellRenderer<Reserva> {
	 
	private static final long serialVersionUID = -1636337442704478324L;
	
	public ActividadesRenderer() {
		setOpaque(true);
	}

	public Component getListCellRendererComponent(JList<? extends Reserva> list, Reserva reserva, int index, boolean isSelected, boolean cellHasFocus) {          
            
        setText(String.format("%s, %s - %s", reserva.getTitular(), reserva.getInicioReserva().toLocalTime(), reserva.getFinReserva().toLocalTime()));   
        
        if (isSelected) {        	
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        }  
        else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        
        return this;
    }
}