package bd;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.omg.CORBA.CODESET_INCOMPATIBLE;

import logica.Actividad;
import logica.Cliente;
import logica.Instalacion;
import logica.Lista;
import logica.Monitor;
import logica.Reserva;
import logica.ReservaCancelada;
import logica.Socio;

public class BaseDatos { 

	public String url;
	public String username;
	public String password;

	public BaseDatos(String url, String username, String password) {

		this.url = url;
		this.username = username;
		this.password = password;	
	}
	private Connection crearConexion() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}	

	//Querys Basicas

	public List<Instalacion> getInstalaciones() {		

		List<Instalacion> instalaciones = new ArrayList<Instalacion>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from INSTALACIONES");							
			rs = ps.executeQuery();

			while(rs.next()){

				String codInstalacion = rs.getString("cod_instalacion");
				String nombre = rs.getString("nombre") ;
				float precio = rs.getFloat("precio_hora") ;

				Instalacion instalacion = new Instalacion(codInstalacion, nombre, precio);
				instalaciones.add(instalacion);			
			}			
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}			
		return instalaciones;	
	}	

	public List<Socio> getSocios() {

		List<Socio> socios = new ArrayList<Socio>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from SOCIOS");							
			rs = ps.executeQuery();

			while(rs.next()) {				

				String dni = rs.getString("dni") ;
				String nombre = rs.getString("nombre") ;
				String apellidos = rs.getString("apellidos") ;

				Socio socio = new Socio(dni,nombre,apellidos);
				socios.add(socio);         
			}
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return socios;     
	} 	

	public List<Reserva> getReservas() {

		List<Reserva> reservas = new ArrayList<Reserva>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from RESERVAS");							
			rs = ps.executeQuery();

			while(rs.next()){

				String codInstalacion = rs.getString("cod_instalacion");
				String titular = rs.getString("titular");
				LocalDateTime inicioReserva = rs.getTimestamp("inicio_reserva").toLocalDateTime() ;
				LocalDateTime finReserva = rs.getTimestamp("fin_reserva").toLocalDateTime();
				LocalDateTime horaEntrada;
				LocalDateTime horaSalida;
				if(rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
					horaEntrada = rs.getTimestamp("hora_llegada").toLocalDateTime();
					horaSalida = rs.getTimestamp("hora_Salida").toLocalDateTime();
				}
				else{
					horaEntrada = null;
					horaSalida = null;
				}
				float precio = rs.getFloat("precio");
				boolean pagado = rs.getBoolean("pagado");
				boolean isCuota = rs.getBoolean("is_cuota");
				String monitor = rs.getString("monitor");
				int numPlazas = rs.getInt("num_plazas");


				Reserva reserva = new Reserva(codInstalacion, titular, inicioReserva, finReserva,
						horaEntrada, horaSalida, precio, pagado, isCuota, monitor, numPlazas);
				reservas.add(reserva);
			}
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	

		return reservas;
	}

	public List<Actividad> getActividades() {

		List<Actividad> actividades = new ArrayList<Actividad>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from ACTIVIDADES");							
			rs = ps.executeQuery();

			while(rs.next()){

				String id_actividad = rs.getString("id_actividad");

				Actividad actividad = new Actividad(id_actividad);
				actividades.add(actividad);
			} 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				
			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return actividades;
	}

	public Actividad getActividadConNombre(String id) {		

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		Actividad actividad = null;

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from ACTIVIDADES where id_actividad = ?");		
			ps.setString(1, id);
			rs = ps.executeQuery();

			if (rs.next()){

				String id_actividad = rs.getString("id_actividad");

				actividad = new Actividad(id_actividad);

			} 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return actividad;
	}

	public List<Lista> getListas() {

		List<Lista> listas = new ArrayList<Lista>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from LISTA");							
			rs = ps.executeQuery();

			while(rs.next()){

				String id_reserva = rs.getString("id_reserva");
				String dni = rs.getString("dni");
				boolean asistencia = rs.getBoolean("asistio");
				Lista lista = new Lista(id_reserva,dni,asistencia);
				listas.add(lista);
			} 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return listas;
	}	

	public List<Lista> getListasPorReserva(Reserva act) {

		List<Lista> listas = new ArrayList<Lista>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from LISTA where id_reserva = ?");	

			ps.setString(1, act.getIdReserva());
			rs = ps.executeQuery();

			while(rs.next()){

				String id_reserva = rs.getString("id_reserva");
				String dni = rs.getString("dni");
				boolean asistencia = rs.getBoolean("asistio");
				Lista lista = new Lista(id_reserva,dni,asistencia);
				listas.add(lista);
			} 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return listas;
	}

	public boolean actualizarAsistenciaSocio(Lista li) {

		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("UPDATE LISTA SET asistio = ? WHERE id_reserva = ? and dni = ? ");	
			ps.setBoolean(1, !li.isAsistencia());
			ps.setString(2, li.getIdReserva());
			ps.setString(3, li.getDni());

			ps.execute();		 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
			return false;
		}
		finally {				

			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return true;
	}


	public Reserva getReservasPorID(String id) throws SQLException{

		Connection con = crearConexion();		
		PreparedStatement ps = con.prepareStatement("SELECT * FROM reservas WHERE id_reserva = ?");			
		ps.setString(1, id);		
		ResultSet rs = ps.executeQuery();

		rs.next();

		String codInstalacion = rs.getString("cod_instalacion");
		String titular = rs.getString("titular");
		LocalDateTime inicioReserva = rs.getTimestamp("inicio_reserva").toLocalDateTime() ;
		LocalDateTime finReserva = rs.getTimestamp("fin_reserva").toLocalDateTime();
		LocalDateTime horaEntrada;
		LocalDateTime horaSalida;

		if(rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
			horaEntrada = rs.getTimestamp("hora_llegada").toLocalDateTime();
			horaSalida = rs.getTimestamp("hora_Salida").toLocalDateTime();
		}
		else{
			horaEntrada = null;
			horaSalida = null;
		}
		float precio = rs.getFloat("precio");
		boolean pagado = rs.getBoolean("pagado");
		boolean isCuota = rs.getBoolean("is_cuota");
		String monitor = rs.getString("monitor");
		int numPlazas = rs.getInt("num_plazas");

		Reserva reserva = new Reserva(codInstalacion, titular, inicioReserva, finReserva,
				horaEntrada, horaSalida, precio, pagado, isCuota, monitor, numPlazas);

		rs.close();
		ps.close();
		con.close();		

		return reserva;
	}

	public List<Reserva> getReservasPorTitular(String titular) throws SQLException{

		List<Reserva> reservas = new ArrayList<Reserva>();

		Connection con = crearConexion();		
		PreparedStatement ps = con.prepareStatement("SELECT * FROM reservas WHERE titular = ?");			
		ps.setString(1, titular);		
		ResultSet rs = ps.executeQuery();

		rs.next();

		String codInstalacion = rs.getString("cod_instalacion");		
		LocalDateTime inicioReserva = rs.getTimestamp("inicio_reserva").toLocalDateTime() ;
		LocalDateTime finReserva = rs.getTimestamp("fin_reserva").toLocalDateTime();
		LocalDateTime horaEntrada;
		LocalDateTime horaSalida;

		if(rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
			horaEntrada = rs.getTimestamp("hora_llegada").toLocalDateTime();
			horaSalida = rs.getTimestamp("hora_Salida").toLocalDateTime();
		}
		else{
			horaEntrada = null;
			horaSalida = null;
		}
		float precio = rs.getFloat("precio");
		boolean pagado = rs.getBoolean("pagado");
		boolean isCuota = rs.getBoolean("is_cuota");
		String monitor = rs.getString("monitor");
		int numPlazas = rs.getInt("num_plazas");


		Reserva reserva = new Reserva(codInstalacion, titular, inicioReserva, finReserva,
				horaEntrada, horaSalida, precio, pagado, isCuota, monitor, numPlazas);
		reservas.add(reserva);

		rs.close();
		ps.close();
		con.close();		

		return reservas;		
	}

	public List<Reserva> getReservaEntreFechasPorInstalacion (String codInstalacion, LocalDate fecha_inicio, LocalDate fecha_final) {

		List<Reserva> reservas = new ArrayList<Reserva>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();	

			ps = con.prepareStatement("SELECT * FROM Reservas WHERE cod_instalacion = ? and inicio_reserva BETWEEN ? AND ? ORDER BY inicio_reserva DESC");
			ps.setString(1, codInstalacion);
			ps.setDate(2, Date.valueOf(fecha_inicio));
			ps.setDate(3, Date.valueOf(fecha_final));
			rs = ps.executeQuery();

			while(rs.next()) {	

				String cod_instalacion = rs.getString("cod_instalacion");
				String titular= rs.getString("titular");
				LocalDateTime inicio_reserva = rs.getTimestamp("inicio_reserva").toLocalDateTime();						
				LocalDateTime fin_reserva = rs.getTimestamp("fin_reserva").toLocalDateTime() ;

				LocalDateTime hora_llegada = null;
				LocalDateTime hora_salida = null;

				if (rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
					hora_llegada  = rs.getTimestamp("hora_llegada").toLocalDateTime();
					hora_salida  = rs.getTimestamp("hora_Salida").toLocalDateTime();
				}
				float precio = rs.getFloat("precio");
				boolean pagado = rs.getBoolean("pagado");
				boolean isCuota = rs.getBoolean("is_cuota");
				String monitor = rs.getString("monitor");
				int numPlazas = rs.getInt("num_plazas");


				Reserva reserva = new Reserva(cod_instalacion, titular, inicio_reserva, fin_reserva,
						hora_llegada, hora_salida, precio, pagado, isCuota, monitor, numPlazas);
				reservas.add(reserva);

			}
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}
		return reservas;
	}	

	public List<Reserva> getReservaActividadesEntreFechasPorInstalacion (String codInstalacion, LocalDate fecha_inicio, LocalDate fecha_final) {

		List<Reserva> reservas = new ArrayList<Reserva>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();	

			ps = con.prepareStatement("SELECT reservas.* FROM reservas, actividades "
					+ "WHERE reservas.titular = actividades.id_actividad and cod_instalacion = ? and inicio_reserva BETWEEN ? AND ? "
					+ "ORDER BY inicio_reserva DESC");

			ps.setString(1, codInstalacion);
			ps.setDate(2, Date.valueOf(fecha_inicio));
			ps.setDate(3, Date.valueOf(fecha_final));
			rs = ps.executeQuery();

			while(rs.next()) {	

				String cod_instalacion = rs.getString("cod_instalacion");
				String titular = rs.getString("titular");
				LocalDateTime inicio_reserva = rs.getTimestamp("inicio_reserva").toLocalDateTime();						
				LocalDateTime fin_reserva = rs.getTimestamp("fin_reserva").toLocalDateTime() ;

				LocalDateTime hora_llegada = null;
				LocalDateTime hora_salida = null;

				if (rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
					hora_llegada  = rs.getTimestamp("hora_llegada").toLocalDateTime();
					hora_salida  = rs.getTimestamp("hora_Salida").toLocalDateTime();
				}
				float precio = rs.getFloat("precio");
				boolean pagado = rs.getBoolean("pagado");
				boolean isCuota = rs.getBoolean("is_cuota");
				String monitor = rs.getString("monitor");
				int numPlazas = rs.getInt("num_plazas");


				Reserva reserva = new Reserva(cod_instalacion, titular, inicio_reserva, fin_reserva,
						hora_llegada, hora_salida, precio, pagado, isCuota, monitor, numPlazas);
				reservas.add(reserva); 
			}
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}
		return reservas;
	}	

	public List<Reserva> getReservasActividadDeHoyPorMonitor(String idMonitor) {

		List<Reserva> reservas = new ArrayList<Reserva>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();	

			ps = con.prepareStatement("SELECT reservas.* FROM reservas, actividades "
					+ "WHERE reservas.titular = actividades.id_actividad and reservas.monitor = ?"
					+ "ORDER BY inicio_reserva ASC");

			ps.setString(1, idMonitor);
			rs = ps.executeQuery();

			while(rs.next()) {	

				LocalDateTime inicio_reserva = rs.getTimestamp("inicio_reserva").toLocalDateTime();				

				if (inicio_reserva.toLocalDate().isAfter(LocalDate.now()))
					break;

				if (inicio_reserva.toLocalDate().equals(LocalDate.now())) {			

					String cod_instalacion = rs.getString("cod_instalacion");
					String titular = rs.getString("titular");										
					LocalDateTime fin_reserva = rs.getTimestamp("fin_reserva").toLocalDateTime() ;

					LocalDateTime hora_llegada = null;
					LocalDateTime hora_salida = null;

					if (rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
						hora_llegada  = rs.getTimestamp("hora_llegada").toLocalDateTime();
						hora_salida  = rs.getTimestamp("hora_Salida").toLocalDateTime();
					}
					float precio = rs.getFloat("precio");
					boolean pagado = rs.getBoolean("pagado");
					boolean isCuota = rs.getBoolean("is_cuota");
					String monitor = rs.getString("monitor");
					int numPlazas = rs.getInt("num_plazas");


					Reserva reserva = new Reserva(cod_instalacion, titular, inicio_reserva, fin_reserva,
							hora_llegada, hora_salida, precio, pagado, isCuota, monitor, numPlazas);
					reservas.add(reserva); 				
				}	
			}
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}
		return reservas;

	}

	public List<Reserva> getReservasMonitorEntreFechas(String idmonitor, LocalDate fecha_inicio, LocalDate fecha_final) {

		List<Reserva> reservas = new ArrayList<Reserva>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();	

			ps = con.prepareStatement("SELECT reservas.* FROM reservas, actividades "
					+ "WHERE reservas.titular = actividades.id_actividad and reservas.monitor = ? and inicio_reserva BETWEEN ? AND ? "
					+ "ORDER BY inicio_reserva DESC");

			ps.setString(1, idmonitor);
			ps.setDate(2, Date.valueOf(fecha_inicio));
			ps.setDate(3, Date.valueOf(fecha_final));
			rs = ps.executeQuery();

			while(rs.next()) {	

				String cod_instalacion = rs.getString("cod_instalacion");
				String titular = rs.getString("titular");
				LocalDateTime inicio_reserva = rs.getTimestamp("inicio_reserva").toLocalDateTime();						
				LocalDateTime fin_reserva = rs.getTimestamp("fin_reserva").toLocalDateTime() ;

				LocalDateTime hora_llegada = null;
				LocalDateTime hora_salida = null;

				if (rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
					hora_llegada  = rs.getTimestamp("hora_llegada").toLocalDateTime();
					hora_salida  = rs.getTimestamp("hora_Salida").toLocalDateTime();
				}
				float precio = rs.getFloat("precio");
				boolean pagado = rs.getBoolean("pagado");
				boolean isCuota = rs.getBoolean("is_cuota");
				String monitor = rs.getString("monitor");
				int numPlazas = rs.getInt("num_plazas");				

				Reserva reserva = new Reserva(cod_instalacion, titular, inicio_reserva, fin_reserva,
						hora_llegada, hora_salida, precio, pagado, isCuota, monitor, numPlazas);
				reservas.add(reserva); }
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}
		return reservas;
	}	

	//Operaciones especificas	
	public Cliente getClienteConDni(String dni) throws SQLException{

		Connection con = crearConexion();
		Statement stat = con.createStatement();

		ResultSet rs = stat.executeQuery("select * from clientes where dni= '" + dni +"'");

		if (rs.next()){

			String nombre = rs.getString("nombre");
			String dni2 = rs.getString("dni") ;
			String fechaNacimiento = rs.getString("fecha_Nacimiento") ;

			return new Cliente(nombre, dni2, fechaNacimiento);

		}
		rs.close();
		stat.close();
		con.close();
		return null;
	}

	public boolean existeSocioConDni(String dni) {

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;
		boolean existe = true;

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from SOCIOS where dni = ?");			
			ps.setString(1, dni);			
			rs = ps.executeQuery();

			if (!rs.next()) 				
				existe = false;	

		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return existe;		
	}	
	public boolean guardarReserva(Reserva rs) {

		PreparedStatement ps = null;	
		Connection con = null;

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("INSERT INTO Reservas VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");		
			ps.setString(1, rs.getIdReserva());
			ps.setString(2, rs.getCodInstalacion());
			ps.setString(3, rs.getTitular());
			ps.setTimestamp(4, Timestamp.valueOf(rs.getInicioReserva()));
			ps.setTimestamp(5, Timestamp.valueOf(rs.getFinReserva()));     
			ps.setTimestamp(6, null);
			ps.setTimestamp(7, null);   
			ps.setFloat(8, rs.getPrecio());
			ps.setBoolean(9, rs.isPagado());
			ps.setBoolean(10, rs.isCuota());	
			ps.setString(11, rs.getMonitor());
			ps.setInt(12, rs.getNumPlazas());
			ps.execute();			
		} 
		catch (SQLException e) { 
			e.printStackTrace();
			return false;
		}
		finally {				
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}					
		return true;
	}  

	public Socio getSocioConDni(String dni) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Socio socio = null;

		try {				
			con = crearConexion();
			ps = con.prepareStatement("select * from socios where dni = ?");			
			ps.setString(1, dni);
			rs = ps.executeQuery();			

			//if (rs.next()) socio = new Socio((rs.getString("dni")),"Hayquecambiar","estoenBaseDatos");	   
			if (rs.next()) socio = new Socio(rs.getString("dni"),rs.getString("nombre"),rs.getString("apellidos"));	   


		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {	

			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return socio;
	}

	public String cancelarReserva(Reserva r) {

		PreparedStatement ps = null;						
		Connection con = null;
		String mensaje = "";

		try {				
			con = crearConexion();
			ps = con.prepareStatement("DELETE FROM reservas WHERE id_reserva = ?");			
			ps.setString(1, r.getIdReserva());			

			if (ps.executeUpdate() > 0)			
				mensaje = "Reserva cancelada correctamente";
			else			
				mensaje = "No existe ninguna reserva que corresponda a los datos introducidos";
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {	

			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return mensaje;
	}


	public void insertReservasCanceladas(Reserva r, boolean isAdmin, String motivo) throws SQLException{

		Connection con = crearConexion();      
		PreparedStatement ps = con.prepareStatement("INSERT INTO RESERVAS_CANCELADAS VALUES (?, ?, ?, ?, ?, ?)");
		ps.setString(1, r.getTitular());
		ps.setTimestamp(2, Timestamp.valueOf(r.getInicioReserva()));
		ps.setTimestamp(3, Timestamp.valueOf(r.getFinReserva()));  
		ps.setString(5, r.getCodInstalacion());

		if(isAdmin == true){				
			ps.setBoolean(4, false);
			ps.setString(6, motivo);
		}
		else {	 
			ps.setBoolean(4, true);			
			ps.setString(6, motivo);			
		}

		ps.execute();		
		ps.close();
		con.close();
	}

	public void updateCuota(String dni,float cuota, String mes, LocalDateTime fecha, String a) throws SQLException{

		LocalDateTime aux2 = LocalDateTime.now();
		Connection con = crearConexion();   
		try{
			PreparedStatement ps = con.prepareStatement("INSERT INTO pagos VALUES (?, ?, ?, ? ,?,?)");

			String aux =  dni+aux2;
			ps.setString(1, aux);
			ps.setString(2, dni);
			ps.setTimestamp(3, Timestamp.valueOf(fecha));
			ps.setString(4, mes);
			ps.setFloat(5, cuota);
			ps.setString(6, a);
			ps.execute();} finally {

				String sentencia = "UPDATE reservas set pagado = ?";
				PreparedStatement psSentencia = con.prepareStatement(sentencia);
				psSentencia.setBoolean(1, true);
				psSentencia.executeUpdate();

			}

	}

	public void setPagado(Reserva rs) throws SQLException {

		Connection con = crearConexion();		
		PreparedStatement ps = con.prepareStatement("UPDATE reservas SET pagado = ? WHERE id_reserva= ?");
		ps.setBoolean(1, rs.isPagado());
		ps.setString(2, rs.getIdReserva());
		ps.execute();
	}


	public void metePersona(String id_reserva,String dni, boolean bol) throws SQLException{

		Connection con = crearConexion();      
		PreparedStatement ps = con.prepareStatement("INSERT INTO lista VALUES (?, ?, ?)");

		ps.setString(1, id_reserva);
		ps.setString(2, dni);
		ps.setBoolean(3, bol);
		ps.execute();
	}


	public void updateHoraEntrada(Reserva reserva) throws SQLException{

		Connection con = crearConexion();
		String sentencia = "UPDATE reservas set hora_llegada = ? where reservas.titular= ?";
		PreparedStatement psSentencia = con.prepareStatement(sentencia);

		psSentencia.setTimestamp(1, Timestamp.valueOf(reserva.getHoraEntrada()));
		psSentencia.setString(2, reserva.getTitular());
		psSentencia.executeUpdate();

		con.close();
	}


	public void updateHoraSalida(Reserva reserva) throws SQLException{

		Connection con = crearConexion();
		String sentencia = "UPDATE reservas set hora_salida = ? where reservas.titular= ?";
		PreparedStatement psSentencia = con.prepareStatement(sentencia);

		psSentencia.setTimestamp(1, Timestamp.valueOf(reserva.getHoraSalida()));
		psSentencia.setString(2, reserva.getTitular());
		psSentencia.executeUpdate();

		con.close();
	}

	public void updateCuotaTotal() throws SQLException{

		Connection con = crearConexion();      

		Statement stat = con.createStatement();
		ResultSet rs = stat.executeQuery("select dni, mes, sum(cantidad) as cant from pagos group by dni, mes");

		while(rs.next()){

			String dni = rs.getString("dni");
			String mes = rs.getString("mes");
			float cantidad = rs.getFloat("cant");      

			PreparedStatement ps = con.prepareStatement("select cantidad from cuota where dni = ? and mes = ?");   
			ps.setString(1, dni);
			ps.setString(2, mes);              
			ResultSet rs2 = ps.executeQuery();

			if (!rs2.next())    {      

				PreparedStatement ps2 = con.prepareStatement("INSERT INTO cuota VALUES (?, ?, ?)");        
				ps2.setString(1, dni);
				ps2.setString(2, mes);
				ps2.setFloat(3, cantidad);
				ps2.execute();
			}
			else {

				float cant = rs.getFloat("cant");
				PreparedStatement ps2 = con.prepareStatement("UPDATE cuota SET cantidad = ? where dni = ? and mes = ?");   
				ps2.setFloat(1,cant);
				ps2.setString(2, dni);
				ps2.setString(3, mes);
				ps2.execute();
			}
		}
		con.close();
	}



	public boolean guardarReservaPlazaActividad(Lista li) {

		PreparedStatement ps = null;	
		Connection con = null;

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("INSERT INTO Lista VALUES (?, ?, ?)");		

			ps.setString(1, li.getIdReserva());
			ps.setString(2, li.getDni());
			ps.setBoolean(3, li.isAsistencia());

			ps.execute();			
		} 
		catch (SQLException e) { 
			e.printStackTrace();
			return false;
		}
		finally {				
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}					
		return true;
	}  



	public List<ReservaCancelada> getReservasCanceladas() {

		List<ReservaCancelada> reservasCanceladas = new ArrayList<ReservaCancelada>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("select * from reservas_canceladas");							
			rs = ps.executeQuery();

			while(rs.next()){

				String dni = rs.getString("dni");
				LocalDateTime inicioReserva = rs.getTimestamp("inicio_reserva").toLocalDateTime() ;
				LocalDateTime finReserva = rs.getTimestamp("fin_reserva").toLocalDateTime();
				boolean avisado = rs.getBoolean("avisado");
				String cod_instalacion = rs.getString("cod_instalacion");
				String causa = rs.getString("causa");

				ReservaCancelada actividad = new ReservaCancelada(dni,inicioReserva,finReserva,avisado,cod_instalacion,causa);
				reservasCanceladas.add(actividad);
			} 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return reservasCanceladas;
	}


	public String cancelarAsistenciaActividad(Lista l) {

		PreparedStatement ps = null;						
		Connection con = null;
		String mensaje = "";

		try {				
			con = crearConexion();
			ps = con.prepareStatement("DELETE FROM lista WHERE id_reserva = ? and dni = ?");			
			ps.setString(1, l.getIdReserva());	
			ps.setString(2, l.getDni());			


			if (ps.executeUpdate() > 0){	
				mensaje = "Asistencia cancelada correctamente";
			}
			else{
				mensaje = "Error en la cancelación";
			}

		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {	

			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return mensaje;
	}








	public String eliminarActividadDiaConcreto(String nombre, LocalDateTime fechaHoraInicio, LocalDateTime fechaHoraFin){

		PreparedStatement ps = null;						
		Connection con = null;
		String mensaje = "";

		try {				
			con = crearConexion();
			ps = con.prepareStatement("DELETE FROM reservas WHERE titular = ? and inicio_reserva = ? and fin_reserva = ?");			
			ps.setString(1, nombre);	
			ps.setTimestamp(2, Timestamp.valueOf(fechaHoraInicio));
			ps.setTimestamp(3, Timestamp.valueOf(fechaHoraFin));



			if (ps.executeUpdate() > 0){	
				mensaje = "Actividad eliminada correctamente";
			}

			else{
				mensaje = "Error en la cancelación";
			}


		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {	

			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}

		return mensaje;

	}

	public ArrayList<Lista> getListaConId(String id) throws SQLException{

		Connection con = crearConexion();
		Statement stat = con.createStatement();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList<Lista> listas = new ArrayList<Lista>();


		ps = con.prepareStatement("SELECT * from lista where id_reserva = ?");			
		ps.setString(1, id);			
		rs = ps.executeQuery();

		if (rs.next()){

			String dni = rs.getString("dni") ;
			boolean asistencia= rs.getBoolean("asistio") ;

			Lista a = new Lista(id, dni, asistencia);
			listas.add(a);
		}
		rs.close();
		stat.close();
		con.close();
		return listas;
	}

	public int getPlazasOcupadas(String id) throws SQLException {
		Connection con = crearConexion();
		Statement stat = con.createStatement();
		ResultSet rs = null;
		PreparedStatement ps = null;	
		int plazas=0;

		ps = con.prepareStatement("select count(*) as plazas from lista where asistio = ? and id_reserva = ?");
		ps.setBoolean(1,true);
		ps.setString(2, id);
		rs = ps.executeQuery();

		while (rs.next()){
			plazas = rs.getInt("plazas") ; }

		rs.close();
		stat.close();
		con.close();
		return plazas;
	}
	public void updateMonitor(String id, String monitor) {

		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("UPDATE Reservas SET monitor = ? WHERE id_reserva = ?");	

			ps.setString(1, monitor);
			ps.setString(2, id);

			ps.execute();		 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	

	}

	public Instalacion getInstalacionConCodigo(String cod) throws SQLException{

		Connection con = crearConexion();
		Statement stat = con.createStatement();

		ResultSet rs = stat.executeQuery("select * from instalaciones where cod_instalacion = '" + cod +"'");

		if (rs.next()){

			String cod_instalacion = rs.getString("cod_instalacion");
			String nombre = rs.getString("nombre") ;
			Float precio_hora = rs.getFloat("precio_hora") ;

			return new Instalacion(cod_instalacion, nombre, precio_hora);

		}
		rs.close();
		stat.close();
		con.close();
		return null;
	}

	public List<Reserva> getReservasPorInstalacion(String cod_instalacion) {

		List<Reserva> reservas = new ArrayList<Reserva>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * FROM reservas WHERE cod_instalacion = ?");			
			ps.setString(1, cod_instalacion);
			rs = ps.executeQuery();

			while(rs.next()){

				String codInstalacion = rs.getString("cod_instalacion");
				String titular = rs.getString("titular");
				LocalDateTime inicioReserva = rs.getTimestamp("inicio_reserva").toLocalDateTime() ;
				LocalDateTime finReserva = rs.getTimestamp("fin_reserva").toLocalDateTime();
				LocalDateTime horaEntrada;
				LocalDateTime horaSalida;
				if(rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
					horaEntrada = rs.getTimestamp("hora_llegada").toLocalDateTime();
					horaSalida = rs.getTimestamp("hora_Salida").toLocalDateTime();
				}
				else{
					horaEntrada = null;
					horaSalida = null;
				}
				float precio = rs.getFloat("precio");
				boolean pagado = rs.getBoolean("pagado");
				boolean isCuota = rs.getBoolean("is_cuota");
				String monitor = rs.getString("monitor");
				int numPlazas = rs.getInt("num_plazas");


				Reserva reserva = new Reserva(codInstalacion, titular, inicioReserva, finReserva,
						horaEntrada, horaSalida, precio, pagado, isCuota, monitor, numPlazas);
				reservas.add(reserva);
			}
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	

		return reservas;
	}

	public String eliminarListaActividadDiaConcreto(String idReserva){

		PreparedStatement ps = null;						
		Connection con = null;
		String mensaje = "";

		try {				
			con = crearConexion();
			ps = con.prepareStatement("DELETE FROM lista WHERE id_reserva = ?");			
			ps.setString(1, idReserva);	

			ps.executeUpdate();


		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {	

			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}

		return mensaje;

	}
	public Reserva getReservaActividad(String idActividad) {
		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		Reserva reserva = null;

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from reservas where titular = ?");		
			ps.setString(1, idActividad);
			rs = ps.executeQuery();

			if (rs.next()){

				String codInstalacion = rs.getString("cod_instalacion");
				String titular = rs.getString("titular");
				LocalDateTime inicioReserva = rs.getTimestamp("inicio_reserva").toLocalDateTime() ;
				LocalDateTime finReserva = rs.getTimestamp("fin_reserva").toLocalDateTime();
				LocalDateTime horaEntrada;
				LocalDateTime horaSalida;
				if(rs.getTimestamp("hora_llegada") != null && rs.getTimestamp("hora_Salida") != null ){
					horaEntrada = rs.getTimestamp("hora_llegada").toLocalDateTime();
					horaSalida = rs.getTimestamp("hora_Salida").toLocalDateTime();
				}
				else{
					horaEntrada = null;
					horaSalida = null;
				}
				float precio = rs.getFloat("precio");
				boolean pagado = rs.getBoolean("pagado");
				boolean isCuota = rs.getBoolean("is_cuota");
				String monitor = rs.getString("monitor");
				int numPlazas = rs.getInt("num_plazas");


				reserva = new Reserva(codInstalacion, titular, inicioReserva, finReserva,
						horaEntrada, horaSalida, precio, pagado, isCuota, monitor, numPlazas);				

			} 
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return reserva;
	}
	
	public List<Monitor> getMonitores() {

		List<Monitor> monitores = new ArrayList<Monitor>();

		ResultSet rs = null;
		PreparedStatement ps = null;	
		Connection con = null;		

		try {			
			con = crearConexion();				
			ps = con.prepareStatement("SELECT * from MONITOR");							
			rs = ps.executeQuery();

			while(rs.next()) {				

				String dni = rs.getString("dni") ;
				String nombre = rs.getString("nombre") ;
				String apellidos = rs.getString("apellidos") ;

				Monitor m = new Monitor(dni,nombre,apellidos);
				monitores.add(m);         
			}
		} 
		catch (SQLException e) { 
			e.printStackTrace(); 
		}
		finally {				

			if (rs != null) { try { rs.close(); } catch (SQLException e) {} }
			if (ps != null) { try { ps.close(); } catch (SQLException e) {} }
			if (con != null) { try { con.close(); } catch (SQLException e) {} }			
		}	
		return monitores;     
	} 	
	
}


