package logica;

import java.time.LocalDateTime;

public class ReservaCancelada {

	public String dni;
	public LocalDateTime inicioReserva;
	public LocalDateTime finReserva; 
	public boolean avisado;
	public String codInstalacion;
	public String causa;
	
	
	
	
	public ReservaCancelada(String dni, LocalDateTime inicioReserva, LocalDateTime finReserva, boolean avisado, 
			String codInstalacion, String causa) {
		this.dni = dni;
		this.inicioReserva = inicioReserva;
		this.finReserva = finReserva;
		this.avisado = avisado;
		this.codInstalacion = codInstalacion;
		this.causa = causa;
	}
	
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public LocalDateTime getInicioReserva() {
		return inicioReserva;
	}
	public void setInicioReserva(LocalDateTime inicioReserva) {
		this.inicioReserva = inicioReserva;
	}
	public LocalDateTime getFinReserva() {
		return finReserva;
	}
	public void setFinReserva(LocalDateTime finReserva) {
		this.finReserva = finReserva;
	}
	public boolean isAvisado() {
		return avisado;
	}
	public void setAvisado(boolean avisado) {
		this.avisado = avisado;
	}
	public String getCodInstalacion() {
		return codInstalacion;
	}
	public void setCodInstalacion(String codInstalacion) {
		this.codInstalacion = codInstalacion;
	}
	public String getCausa() {
		return causa;
	}
	public void setCausa(String causa) {
		this.causa = causa;
	}
	
	
	
	
	
	
	
	
	
	
	
}
