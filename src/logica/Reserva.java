package logica;

import java.time.LocalDateTime;

public class Reserva {
	 
	public String titular;
	public String codInstalacion;		
	private String idReserva;
	private float precio;
	private boolean pagado;
	public boolean isCuota;
	public String month;
	public String monitor;
	public int numPlazas;
		
	private LocalDateTime inicioReserva; //http://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html
	private LocalDateTime finReserva; 
	
	private LocalDateTime horaEntrada; //hora entrada a la actividad/instalacion
	private LocalDateTime horaSalida; //hora salida de actividad/instalacion
	
	//*Setters y Getters
	public String getTitular() { return titular; }
	public void setReservante(String titular) {this.titular = titular;}		
	
	public String getCodInstalacion() {	return codInstalacion; }
	public void setCodInstalacion(String cod_instalacion) { this.codInstalacion = cod_instalacion;}
	
	public LocalDateTime getInicioReserva() { return inicioReserva; }
	public void setInicioReserva(LocalDateTime inicioReserva) {	this.inicioReserva = inicioReserva;	}	
	
	public LocalDateTime getFinReserva() { return finReserva; }	
	public void setFinReserva(LocalDateTime finReserva) { this.finReserva = finReserva; }	
	
	public LocalDateTime getHoraEntrada() { return horaEntrada; }
	public void setHoraEntrada(LocalDateTime horaEntrada) {	this.horaEntrada = horaEntrada;	}	
	
	public LocalDateTime getHoraSalida() { return horaSalida; }
	public void setHoraSalida(LocalDateTime horaSalida) {	this.horaSalida = horaSalida;	}	
	
	public String getIdReserva() { return this.idReserva; }
	public void setIdReserva() {
		this.idReserva = inicioReserva.toString()+ "-" + finReserva.toString()+ "-" + titular + "-" + codInstalacion;
	}
		
	public boolean isPagado() {  return pagado;	}
	public void setPagado(boolean pagado) {	this.pagado = pagado; }
	
	public float getPrecio() {  return precio; }
	public void setPrecio(float precio) {	this.precio = precio; }
	
	public boolean isCuota() { return isCuota;}
	public void setIsCuota(boolean isCuota) { this.isCuota = isCuota;}
	
	public String getMonitor() { return monitor; }
	public void setMonitor(String monitor) { this.monitor = monitor; }
	
	public int getNumPlazas() { return numPlazas; }
	public void setNumPlazas(int numPlazas) { this.numPlazas = numPlazas; }
	
	public Reserva(String idInstalacion, String reservante, LocalDateTime inicioReserva, LocalDateTime finReserva, LocalDateTime horaEntrada, LocalDateTime horaSalida, float precio, boolean pagado, boolean isCuota, String monitor, int numPlazas) {
		
		this.codInstalacion = idInstalacion;
		this.titular = reservante;		
		this.pagado = pagado;
		this.inicioReserva = inicioReserva;
		this.finReserva = finReserva;
		this.horaEntrada = horaEntrada;
		this.horaSalida = horaSalida;
		this.precio = precio;	
		this.isCuota = isCuota;
		this.monitor = monitor;
		this.numPlazas = numPlazas;
		
		this.setIdReserva();
	}
	
	public String mostrarDetallesParaInstalaciones(boolean isAdmin, Socio socio) {
	
		String pagado;
		if (isPagado()) pagado = "S�";
		else pagado = "No";
		
		if (!isAdmin || socio != null) 				
			return String.format("Datos de la reserva:\n\n- Titular de Reserva: %s\n- Instalaci�n: %s\n- Fecha: %s\n- Desde: %s\n- Hasta: %s\n- Precio: %.0f\n- Pagado: %s", 
					titular, codInstalacion, inicioReserva.toLocalDate(), inicioReserva.toLocalTime(), finReserva.toLocalTime(), precio, pagado);
		
		return  String.format("Datos de la reserva:\n\n- Titular de Reserva: %s\n- Instalaci�n: %s\n- Fecha: %s\n- Desde: %s\n- Hasta: %s", 
				titular, codInstalacion, inicioReserva.toLocalDate(), inicioReserva.toLocalTime(), finReserva.toLocalTime());	
    }
	
	public String mostrarDetallesParaActividades() {		
		 			
		return String.format("- Actividad: %s\n- Instalaci�n: %s\n- Fecha: %s\n- Desde: %s\n- Hasta: %s\n- Monitor: %s\n- N�mero de plazas: %d\n", 
				titular, codInstalacion, inicioReserva.toLocalDate(), inicioReserva.toLocalTime(), finReserva.toLocalTime(), monitor, numPlazas);
    }
	
    public int getMonth(){
    	int mes = inicioReserva.getMonthValue();
    	return mes;
    }	
}