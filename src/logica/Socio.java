package logica;

import java.time.Month;
import java.util.HashMap;

public class Socio {
 
	public String dni;
	public String nombre;
	public String apellidos;
	//HashMap<Month,Float> pagos;
	
	public boolean enCuenta; //true si la reserva se cobro en cuenta, false en efectivo

	
	
	public Socio(String dni, String nombre, String apellidos){
		this.dni = dni;
		this.nombre=nombre;
		this.apellidos=apellidos;
		setEnCuenta(true);

	}		
	

	

	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	public boolean isEnCuenta() {
		return enCuenta;
	}
	public void setEnCuenta(boolean enCuenta) {
		this.enCuenta = enCuenta;
	}
}
