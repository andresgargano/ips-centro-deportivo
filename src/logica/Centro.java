package logica;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import bd.BaseDatos;

public class Centro { 

	private boolean isAdmin;	
	private BaseDatos baseDatos;

	//Setters and Getters	
	public int getPlazasOcupadas(String id) throws SQLException {return baseDatos.getPlazasOcupadas(id);}
	public List<Lista> getListasPorId(String id) throws SQLException {return baseDatos.getListaConId(id);}
	public List<Lista> getLista() {return  baseDatos.getListas();}
	public List<Socio> getSocios() { return  baseDatos.getSocios(); }
	public List<Instalacion> getInstalaciones() { return baseDatos.getInstalaciones();	}
	public List<Reserva> getReservas() { return  baseDatos.getReservas(); }
	public List<Actividad> getActividades() { return baseDatos.getActividades(); }
	public List<ReservaCancelada> getReservasCanceladas() { return baseDatos.getReservasCanceladas(); }
	public List<Monitor> getMonitor() {return baseDatos.getMonitores();}


	public BaseDatos getBaseDatos() { return this.baseDatos; }

	public boolean isAdmin() { return isAdmin; }
	public void setAdmin(boolean isAdmin) {	this.isAdmin = isAdmin; }




	//Constructor
	public Centro() {		
		baseDatos = new BaseDatos("jdbc:hsqldb:hsql://localhost/labdb", "SA", "");
		isAdmin = true;
	}
}

