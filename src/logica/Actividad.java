package logica;

public class Actividad {
	
	String id_actividad;
	
	public Actividad(String id_actividad) {
		super();
		this.id_actividad = id_actividad;
	}

	public String getIdActividad() {
		return id_actividad;
	}

	public void setIdActividad(String id_actividad) {
		this.id_actividad = id_actividad;
	}		
	
	@Override
	public String toString() {
		return this.id_actividad;
	}
}
