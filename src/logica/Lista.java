package logica;

public class Lista {
	
	String id_reserva;
	String dni;
	boolean asistencia;	
	
	public Lista(String id_reserva, String dni, boolean asistencia) {
		super();
		this.id_reserva = id_reserva;
		this.dni = dni;
		this.asistencia = asistencia;
	}
	public String getIdReserva() {
		return id_reserva;
	}
	public void setIdReserva(String id_reserva) {
		this.id_reserva = id_reserva;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public boolean isAsistencia() {
		return asistencia;
	}
	public void setAsistencia(boolean asistencia) {
		this.asistencia = asistencia;
	}
	
	@Override
	public String toString() {		
		
		String asistio = "No";
		if (asistencia) asistio = "S�"; 
				
		return String.format("DNI: %s - �Asisti�? %s", dni, asistio);
	}
}
