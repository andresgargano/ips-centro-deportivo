 package logica;

import java.time.*;
import java.util.List;

import bd.BaseDatos;
 
public class Comprobaciones {	

	public static Centro centro = new Centro();

	public static String comprobarFechaPasada(LocalDate fecha, boolean isAdmin) {
		if (fecha.isBefore(LocalDate.now()) && !isAdmin) 
			return "- Para poder ver fechas ya pasadas debe ser administrador.\n";
		return "";					
	}

	public static String comprobarFechasValidas(LocalDateTime horaInicio, LocalDateTime horaFin, String dni, boolean isAdmin) {

		if (horaFin.isEqual(horaInicio) || horaFin.isBefore(horaInicio)) 
			return "- La hora final debe ser despu�s que la final\n";			

		if (isAdmin) {
			if (LocalDateTime.now().plusMinutes(5).isAfter(horaInicio))
				return "- No se puede reservar ni cancelar con menos de 5 minutos de antelaci�n\n";						
		}		
		else {			
			if (LocalDateTime.now().plusHours(1).isAfter(horaInicio))
				return "- No se puede reservar ni cancelar con menos de 1 hora de antelaci�n\n";	

			if(LocalDateTime.now().plusDays(15).isBefore(horaInicio))
				return "- No se puede reservar con mas de 15 dias de antelacion";				
		}
		if (!dni.equals("admin")) { //La reserva es de/para un socio

			LocalDateTime horasDiferencia = horaFin.minusHours(2).plusMinutes(1);
			if (horasDiferencia.isAfter(horaInicio)) //Hay mas de dos horas de diferencia
				return "- La reserva de un socio no puede durar m�s de dos horas\n";
		}	
		return "";
	}
	public static String comprobarDniSocio(String dni, BaseDatos base) {

		if (!base.existeSocioConDni(dni))
			return "- El dni introducido no est� dado de alta en el sistema\n";

		return "";	
	}

	public static int calcularNumeroDeHorasReservadas(LocalDateTime horaInicio, LocalDateTime horaFin) {

		LocalDateTime aux = horaInicio.plusHours(1);
		if (aux.isAfter(horaFin)) 
			return 1;

		return 2;
	} 

	public static String comprobarColisionEnReserva(LocalDateTime horainicio, LocalDateTime horaFin){

		List<Reserva> reservas = centro.getBaseDatos().getReservas();

		for(int j=0; j< reservas.size(); j++){

			LocalDateTime inicioR = reservas.get(j).getInicioReserva();
			LocalDateTime finR = reservas.get(j).getFinReserva();

			if (horainicio.isEqual(inicioR) || horainicio.plusMinutes(59).isEqual(finR) || inicioR.plusMinutes(59).isEqual(horaFin) || horaFin.isEqual(finR)) 
				return  "- Ya existe una reserva para en alguna de las horas involucradas\n";
		}
		return "";
	}

	public static Reserva getReservaEnColision(LocalDateTime h1i, LocalDateTime h1f, String cod_instalacion){

		List<Reserva> reservas = centro.getBaseDatos().getReservasPorInstalacion(cod_instalacion);

		for(int j=0; j< reservas.size(); j++) {

			LocalDateTime h2i = reservas.get(j).getInicioReserva();
			LocalDateTime h2f = reservas.get(j).getFinReserva();

			if ((h1i.isBefore(h2i) && h1f.isAfter(h2i)) || (h2i.isBefore(h1i) && h2f.isAfter(h1i)) || (h1i.isBefore(h2f) && h1f.isAfter(h2f)) || (h2i.isBefore(h1f) && h2f.isAfter(h1f))
					|| h1i.equals(h2i) || h1f.equals(h2f)) 
				
				return  reservas.get(j);
		}
		return null;
	}
	 
	

	public static String comprobarNumeroPlazas(Reserva re) {
		List<Lista> lista = centro.getBaseDatos().getListas();
		int plazasDisponibles = re.getNumPlazas();
		int aux=0;
		for (Lista l :lista)
			if(l.getIdReserva().equals(re.getIdReserva())){
				aux+=1;
			}
		if(plazasDisponibles-aux==0)
			return "No quedan plazas disponibles";
		else			
			return "";
	}

	public static String comprobarAntelacionActividades(Reserva re) {
		String error = "";	
		List<Actividad> actividades = centro.getBaseDatos().getActividades();
		for(int j = 0; j<actividades.size();j++){
			if(re.getTitular().equals(actividades.get(j).getIdActividad())){				
				if (re.getInicioReserva().minusHours(24).isAfter(LocalDateTime.now())){
					error = "No se puede reservar con mas de un dia de antelacion";
				}
				else if(re.getInicioReserva().minusHours(1).minusMinutes(1).isBefore(LocalDateTime.now())){
					error = "No se puede reservar plaza con menos de 1h de antelaci�n";
				}
				else
					error= "";
			}			
		}



		return error; }
	
	

	public static String comprobarFechaHoraCancelacion(LocalDateTime fechaCompletaInicio, LocalDateTime fechaCompletaFin, String dni, boolean isAdmin) {

		LocalDateTime fechaHoraActual = LocalDateTime.now();

		int a�oSeleccionado = fechaCompletaInicio.getYear();
		int diaSeleccionado = fechaCompletaInicio.getDayOfMonth();
		int mesSeleccionado = fechaCompletaInicio.getMonth().getValue();
		
		int a�oAhora = fechaHoraActual.getYear();
		int diaAhora = fechaHoraActual.getDayOfMonth();
		int mesAhora = fechaHoraActual.getMonth().getValue();
		
		if (fechaCompletaFin.isEqual(fechaCompletaInicio) || fechaCompletaFin.isBefore(fechaCompletaInicio)) {
			return "- La hora final debe ser despu�s que la final\n";		
		}

		
		if(a�oSeleccionado == a�oAhora && diaSeleccionado == diaAhora && mesSeleccionado == mesAhora){
				if (LocalDateTime.now().plusMinutes(5).isAfter(fechaCompletaInicio)){
					return "- No se puede cancelar actividades pasadas o con menos de 5 minutos de antelaci�n\n";	
				}
		}
			
		if(a�oSeleccionado < a�oAhora || (diaSeleccionado < diaAhora && mesSeleccionado < mesAhora)){
			return "- No se pueden cancelar eventos pasados";	
		}
		
		
		

		return "";
	}
	
	
	
	}
	



