package logica;

import java.util.HashMap;
import java.util.TreeMap;

public class Instalacion {
	
	private String codInstalacion;
	private String nombre;
	private float precio;
	
	public boolean disponible;	
	
	
	//Setters y Getters
	public String getCodInstalacion() { return codInstalacion; }
	public void setCodInstalacion(String codInstalacion) { this.codInstalacion = codInstalacion; }
	
	public String getNombre() { return nombre; }
	public void setNombre(String nombre) { this.nombre = nombre; }
	
	public float getPrecio() { return precio; }
	public void setPrecio(float precio) {
		if (precio > 0) this.precio = precio;
	}	
	
	public boolean isDisponible() { return disponible; }
	public void setDisponible(boolean disponible) { this.disponible = disponible; }	
	
	//Constructor
	public Instalacion(String codInstalacion, String nombre, float precio){
		this.codInstalacion = codInstalacion;
		this.nombre = nombre;
		this.precio = precio;		
	}
	
	@Override
	public String toString() {		
		if (precio != 0)			return String.format("%s - %.0f �/hora", this.getNombre(), this.getPrecio());
		else						return String.format("%s", this.getNombre());
	}
}
